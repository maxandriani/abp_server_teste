﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Booking.Places.Contracts;
using Booking.Places.Dto;
using Booking.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Places
{
    [AbpAuthorize]
    public class PlaceCrudService : AsyncCrudAppService<Place, PlaceDto, long, PagedAndSortedPlaceRequestDto, CreatePlaceDto, UpdatePlaceDto, EntityDto<long>, EntityDto<long>>, IPlaceCrudService
    {
        protected readonly IAuditedQueryParserService AuditedQueryParser;

        public PlaceCrudService(
            IRepository<Place, long> placeRepo,
            IAuditedQueryParserService auditedQueryParser
        ) : base(placeRepo)
        {
            AuditedQueryParser = auditedQueryParser;
        }

        #region Query parsers
        protected IQueryable<Place> ParseSearch(IQueryable<Place> query, PagedAndSortedPlaceRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Search))
            {
                query = query.Where(p => p.Name.ToLower().Contains(input.Search.ToLower()));
            }

            return query;
        }

        protected IQueryable<Place> ParseSourceSearch(IQueryable<Place> query, PagedAndSortedPlaceRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Source))
            {
                query = query.SelectMany(p => p.Sources.Where(s => s.Name.ToLower().Contains(input.Source.ToLower()))).Select(x => x.Place);
            }

            return query;
        }

        #endregion

        #region Filters
        protected override IQueryable<Place> CreateFilteredQuery(PagedAndSortedPlaceRequestDto input)
        {
            var places = (input.ExpandSource)
                ? Repository.GetAllIncluding(p => p.Sources)
                : Repository.GetAll();

            places = AuditedQueryParser.Parse(places, input).Cast<Place>();
            places = ParseSearch(places, input);
            places = ParseSourceSearch(places, input);

            return places;
        }
        #endregion

        #region Custom Queries
        public async Task<PlaceDto> GetPlaceFullAsync(EntityDto<long> input)
        {
            var place = Repository
                .GetAllIncluding(p => p.Sources)
                .First();

            return MapToEntityDto(place);
        }
        #endregion

    }
}
