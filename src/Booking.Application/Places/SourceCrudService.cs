﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Booking.Places.Contracts;
using Booking.Places.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Places
{
    [AbpAuthorize]
    public class SourceCrudService : AsyncCrudAppService<Source, SourceDto, long, PagedAndSortedSourceRequestDto, SourceDto, UpdateSourceDto, GetSourceDto, GetSourceDto>, ISourceCrudService
    {
        #region Exceptions
        protected const string YouCanNotSearchSourcesWOPlaces = "places.sources.youCanNotSearchSourcesWOPlace";
        protected const string YouCanNotManipulateSourcesWoPlace = "places.sources.youCanNotManipulateSourcesWoPlace";
        #endregion

        protected readonly IRepository<Place, long> PlaceRepo;

        public SourceCrudService(
            IRepository<Source, long> sourceRepo,
            IRepository<Place, long> placeRepo
        ) : base(sourceRepo)
        {
            PlaceRepo = placeRepo;
        }

        #region Query parsers
        protected IQueryable<Source> ApplySearchFilter(IQueryable<Source> query, PagedAndSortedSourceRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Search))
            {
                query = query.Where(s => s.Name.ToLower().Contains(input.Search.ToLower()) || s.Url.ToLower().Contains(input.Search.ToLower()));
            }

            return query;
        }

        protected IQueryable<Source> ApplyPlaceFilter(IQueryable<Source> query, PagedAndSortedSourceRequestDto input)
        {
            return query.Where(s => s.PlaceId == input.Place.Value && s.Place.IsDeleted == false);
        }
        #endregion

        #region Filters
        protected override IQueryable<Source> CreateFilteredQuery(PagedAndSortedSourceRequestDto input)
        {
            var query = Repository.GetAll();

            query = ApplySearchFilter(query, input);
            query = ApplyPlaceFilter(query, input);

            return query;
        }
        #endregion

        #region Crud Overrides
        public override async Task<SourceDto> Create(SourceDto input)
        {
            var place = await PlaceRepo.GetAsync(input.PlaceId);
            var source = MapToEntity(input);

            place.Sources.Add(source);
            PlaceRepo.Update(place);

            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(source);
        }

        public override async Task Delete(GetSourceDto input)
        {
            var place = await PlaceRepo.GetAsync(input.PlaceId);
            var source = Repository.Get(input.Id);

            CheckPlaceSourceRelation(place, source);

            await base.Delete(input);
        }

        public override async Task<SourceDto> Update(UpdateSourceDto input)
        {
            var place = await PlaceRepo.GetAsync(input.PlaceId);
            var source = await Repository.GetAsync(input.Id);

            source = ObjectMapper.Map<UpdateSourceDto, Source>(input, source);

            CheckPlaceSourceRelation(place, source);

            return await base.Update(input);
        }

        protected void CheckPlaceSourceRelation(Place place, Source source)
        {
            if (!place.Sources.Contains(source))
            {
                throw new AbpValidationException(YouCanNotManipulateSourcesWoPlace);
            }
        }

        public override async Task<SourceDto> Get(GetSourceDto input)
        {
            var place = await PlaceRepo.GetAsync(input.PlaceId);

            var source = Repository
                .GetAll()
                .Where(s => s.Place.Id == input.PlaceId && s.Id == input.Id && s.Place.IsDeleted != true)
                .First();

            return MapToEntityDto(source);
        }

        public override async Task<PagedResultDto<SourceDto>> GetAll(PagedAndSortedSourceRequestDto input)
        {
            if (!input.Place.HasValue || input.Place.Value == 0)
            {
                throw new AbpValidationException(YouCanNotSearchSourcesWOPlaces);
            }

            var place = await PlaceRepo.GetAsync(input.Place.Value);

            return await base.GetAll(input);
        }
        #endregion
    }
}
