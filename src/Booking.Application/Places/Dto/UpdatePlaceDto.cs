﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Place))]
    public class UpdatePlaceDto : EntityDto<long>
    {
        #region Business properties
        [StringLength(PlaceRules.MaxNameLength)]
        [Required]
        public string Name { get; set; }

        [StringLength(PlaceRules.MaxDescriptionLength)]
        public string Description { get; set; }
        #endregion

        #region Relational properties
        // public ICollection<UpdateSourceDto> Sources { get; set; }
        #endregion
    }
}
