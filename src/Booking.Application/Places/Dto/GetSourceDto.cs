﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Source))]
    public class GetSourceDto : EntityDto<long>
    {
        [Required]
        public long PlaceId { get; set; }
    }
}
