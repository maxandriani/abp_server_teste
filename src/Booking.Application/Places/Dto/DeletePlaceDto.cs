﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Place))]
    public class DeletePlaceDto : EntityDto<long>
    {
    }
}
