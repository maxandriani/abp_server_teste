﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Places.Dto
{
    public class PagedAndSortedSourceRequestDto : PagedAndSortedResultRequestDto
    {
        public string Search { get; set; }
        public long? Place { get; set; }
    }
}
