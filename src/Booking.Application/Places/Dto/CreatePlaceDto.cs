﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Place))]
    public class CreatePlaceDto
    {
        #region Business properties
        [StringLength(PlaceRules.MaxNameLength)]
        [Required]
        public virtual string Name { get; set; }

        [StringLength(PlaceRules.MaxDescriptionLength)]
        public virtual string Description { get; set; }
        #endregion

        #region Relational properties
        public virtual ICollection<CreatePlaceSourceDto> Sources { get; set; }
        #endregion

    }
}
