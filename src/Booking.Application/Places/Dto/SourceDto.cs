﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Source))]
    public class SourceDto : EntityDto<long>
    {
        
        #region Business properties
        [StringLength(SourceRules.MaxNameLength)]
        public string Name { get; set; }

        [StringLength(SourceRules.MaxUrlLength)]
        [Required]
        [Url]
        public string Url { get; set; }
        #endregion

        #region Relational properties
        [Required]
        public long PlaceId { get; set; }
        #endregion

    }
}
