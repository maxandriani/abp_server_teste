﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Booking.Places.Dto
{
    [AutoMapTo(typeof(Place))]
    public class PlaceDto : EntityDto<long>
    {
        #region Audition properties
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        #endregion

        #region Business properties
        [StringLength(PlaceRules.MaxNameLength)]
        [Required]
        public string Name { get; set; }

        [StringLength(PlaceRules.MaxDescriptionLength)]
        public string Description { get; set; }
        #endregion

        #region Relational properties
        public ICollection<SourceDto> Sources { get; set; }
        #endregion
    }
}
