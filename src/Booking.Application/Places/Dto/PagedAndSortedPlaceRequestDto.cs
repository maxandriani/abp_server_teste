﻿using Abp.Application.Services.Dto;
using Booking.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Places.Dto
{
    public class PagedAndSortedPlaceRequestDto : PagedAndSortedResultRequestDto, IAuditedRequestDto
    {
        #region Audition properties
        public long? CreatorUser { get; set; }
        public DateTime? CreationTime { get; set; }
        public DateTime? CreationStart { get; set; }
        public DateTime? CreationEnd { get; set; }
        public long? LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? LastModificationStart { get; set; }
        public DateTime? LastModificationEnd { get; set; }
        #endregion

        #region Business properties
        public string Search { get; set; }
        #endregion

        #region Relational properties
        public bool ExpandSource { get; set; }
        public string Source { get; set; }
        #endregion
    }
}
