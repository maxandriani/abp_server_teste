﻿using Abp.Application.Services;
using Booking.Places.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Places.Contracts
{
    public interface ISourceCrudService : IAsyncCrudAppService<SourceDto, long, PagedAndSortedSourceRequestDto, SourceDto, UpdateSourceDto, GetSourceDto, GetSourceDto>
    {

    }
}
