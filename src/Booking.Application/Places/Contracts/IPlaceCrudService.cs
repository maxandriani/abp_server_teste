﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Booking.Places.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Places.Contracts
{
    public interface IPlaceCrudService : IAsyncCrudAppService<PlaceDto, long, PagedAndSortedPlaceRequestDto, CreatePlaceDto, UpdatePlaceDto, EntityDto<long>, EntityDto<long>>
    {

        Task<PlaceDto> GetPlaceFullAsync(EntityDto<long> input);

    }
}
