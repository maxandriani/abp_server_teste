﻿using System.Threading.Tasks;
using Booking.Configuration.Dto;

namespace Booking.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
