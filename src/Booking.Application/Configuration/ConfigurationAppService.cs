﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Booking.Configuration.Dto;

namespace Booking.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : BookingAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
