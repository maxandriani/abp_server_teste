﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Booking.Contracts;
using System.Threading.Tasks;

namespace Booking
{
    public static class BookingReadOnlyAppServiceBusiness
    {
        public const string PermissionError = "general.permissionError";
    }

    public abstract class BookingReadOnlyAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TGetInput> : BookingAppServiceBase, IBookingReadOnlyAppService<TEntityDto, TPrimaryKey, TGetAllInput, TGetInput>
        where TEntity : class, IEntity<TPrimaryKey>
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TGetInput : IEntityDto<TPrimaryKey>
    {
        protected virtual string GetAllPermissionName { get; set; }
        protected virtual string GetPermissionName { get; set; }
        
        protected readonly IRepository<TEntity, TPrimaryKey> Repository;

        protected BookingReadOnlyAppService(IRepository<TEntity, TPrimaryKey> repository) : base()
        {
            Repository = repository;
        }

        public virtual async Task<TEntityDto> Get(TGetInput input)
        {
            var entity = Repository.Get(input.Id);
            return ObjectMapper.Map<TEntityDto>(entity);
        }

        public virtual async Task<PagedResultDto<TEntityDto>> GetAll(TGetAllInput input)
        {
            await CheckGetAllPermission();

            var query = CreateFilteredQuery(input);

            var totalCount = query.Count();

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = query.ToList();

            return new PagedResultDto<TEntityDto>(
                totalCount,
                entities.Select(ObjectMapper.Map<TEntityDto>).ToList()
            );
        }

        #region Query
        protected virtual IQueryable<TEntity> CreateFilteredQuery(TGetAllInput input)
        {
            return Repository.GetAll();
        }

        /// <summary>
        /// Should apply sorting if needed.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="input">The input.</param>
        protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> query, TGetAllInput input)
        {
            //Try to sort query if available
            var sortInput = input as ISortedResultRequest;
            if (sortInput != null)
            {
                if (!String.IsNullOrWhiteSpace(sortInput.Sorting))
                {
                    return query.OrderBy<TEntity>(sortInput.Sorting);
                }
            }

            //IQueryable.Task requires sorting, so we should sort if Take will be used.
            if (input is ILimitedResultRequest)
            {
                return query.OrderByDescending(e => e.Id);
            }

            //No sorting
            return query;
        }

        /// <summary>
        /// Should apply paging if needed.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="input">The input.</param>
        protected virtual IQueryable<TEntity> ApplyPaging(IQueryable<TEntity> query, TGetAllInput input)
        {
            //Try to use paging if available
            var pagedInput = input as IPagedResultRequest;
            if (pagedInput != null)
            {
                return query.PageBy(pagedInput);
            }

            //Try to limit query result if available
            var limitedInput = input as ILimitedResultRequest;
            if (limitedInput != null)
            {
                return query.Take(limitedInput.MaxResultCount);
            }

            //No paging
            return query;
        }

        protected virtual IQueryable<TEntity> ApplyPaging(IQueryable<TEntity> query, IPagedResultRequest input)
        {
            return query;
        }
        #endregion

        #region Permissions
        protected virtual async Task CheckGetAllPermission()
        {
            if (!String.IsNullOrEmpty(GetAllPermissionName))
            {
                await CheckPermission(GetAllPermissionName);
            }
        }

        protected virtual async Task CheckGetPermission()
        {
            if (!String.IsNullOrEmpty(GetPermissionName))
            {
                await CheckPermission(GetPermissionName);
            }
        }

        protected virtual async Task CheckPermission(string permissionName)
        {
            if (await PermissionChecker.IsGrantedAsync(permissionName) == false)
            {
                throw new AbpAuthorizationException(BookingReadOnlyAppServiceBusiness.PermissionError + "." + permissionName);
            } 
        }
        #endregion
    }
}
