﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Linq;

namespace Booking.Utils
{
    public class AuditedQueryParserService: BookingAppServiceBase, IAuditedQueryParserService
    {
        public AuditedQueryParserService()
        {
        }

        // ICreationAudited, IModificationAudited, IFullAdudited
        protected IQueryable<ICreationAudited> ParseCreationUser(IQueryable<ICreationAudited> query, IAuditedRequestDto input)
        {
            if (input.CreatorUser.HasValue)
            {
                query = query.Where(p => p.CreatorUserId == input.CreatorUser);
            }

            return query;
        }

        protected IQueryable<IModificationAudited> ParseLastModifierUser(IQueryable<IModificationAudited> query, IAuditedRequestDto input)
        {
            if (input.CreatorUser.HasValue)
            {
                query = query.Where(p => p.LastModifierUserId == input.LastModifierUser);
            }

            return query;
        }

        protected IQueryable<ICreationAudited> ParseCreationTime(IQueryable<ICreationAudited> query, IAuditedRequestDto input)
        {
            if (input.CreationTime.HasValue)
            {
                input.CreationStart = new DateTime(
                    input.CreationTime.Value.Year,
                    input.CreationTime.Value.Month,
                    input.CreationTime.Value.Day,
                    0, 0, 0
                );
                input.CreationEnd = input.CreationStart.Value.AddDays(1.0);
            }

            if (input.CreationStart.HasValue)
            {
                query = query.Where(p => p.CreationTime >= input.CreationStart.Value);
            }

            if (input.CreationEnd.HasValue)
            {
                query = query.Where(p => p.CreationTime < input.CreationEnd.Value);
            }

            return query;
        }

        protected IQueryable<IModificationAudited> ParseLastModifiedTime(IQueryable<IModificationAudited> query, IAuditedRequestDto input)
        {
            if (input.LastModificationTime.HasValue)
            {
                input.LastModificationStart = new DateTime(
                    input.LastModificationTime.Value.Year,
                    input.LastModificationTime.Value.Month,
                    input.LastModificationTime.Value.Day,
                    0, 0, 0
                );
                input.LastModificationEnd = input.LastModificationStart.Value.AddDays(1.0);
            }

            if (input.LastModificationStart.HasValue)
            {
                query = query.Where(p => p.LastModificationTime >= input.LastModificationStart.Value);
            }

            if (input.LastModificationEnd.HasValue)
            {
                query = query.Where(p => p.LastModificationTime < input.LastModificationEnd.Value);
            }

            return query;
        }

        public IQueryable<IFullAudited> Parse(IQueryable<IFullAudited> query, IAuditedRequestDto filters)
        {
            query = ParseCreationAudited(query, filters).Cast<IFullAudited>();
            query = ParseModifiedAudited(query, filters).Cast<IFullAudited>();
            return query;
        }

        public IQueryable<ICreationAudited> ParseCreationAudited(IQueryable<ICreationAudited> query, IAuditedRequestDto filters)
        {
            query = ParseCreationUser(query, filters);
            query = ParseCreationTime(query, filters);

            return query;
        }

        public IQueryable<IModificationAudited> ParseModifiedAudited(IQueryable<IModificationAudited> query, IAuditedRequestDto filters)
        {
            query = ParseLastModifierUser(query, filters);
            query = ParseLastModifiedTime(query, filters);

            return query;
        }
    }
}
