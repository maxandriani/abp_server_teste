﻿using System;

namespace Booking.Utils
{
    public interface IAuditedRequestDto : ICreationAuditedRequestDto, IModifiedAuditedRequestDto
    {
    }
}
