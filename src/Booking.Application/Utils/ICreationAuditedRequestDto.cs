﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Utils
{
    public interface ICreationAuditedRequestDto
    {
        long? CreatorUser { get; set; }
        DateTime? CreationTime { get; set; }
        DateTime? CreationStart { get; set; }
        DateTime? CreationEnd { get; set; }
    }
}
