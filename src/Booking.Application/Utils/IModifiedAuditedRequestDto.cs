﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Utils
{
    public interface IModifiedAuditedRequestDto
    {
        long? LastModifierUser { get; set; }
        DateTime? LastModificationTime { get; set; }
        DateTime? LastModificationStart { get; set; }
        DateTime? LastModificationEnd { get; set; }
    }
}
