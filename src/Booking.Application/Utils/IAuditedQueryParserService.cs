﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Booking.Utils
{
    public interface IAuditedQueryParserService
    {
        // ICreationAudited, IModificationAudited, IDeletionAudited
        IQueryable<IFullAudited> Parse(IQueryable<IFullAudited> query, IAuditedRequestDto filters);
        IQueryable<ICreationAudited> ParseCreationAudited(IQueryable<ICreationAudited> query, IAuditedRequestDto filters);
        IQueryable<IModificationAudited> ParseModifiedAudited(IQueryable<IModificationAudited> query, IAuditedRequestDto filters);
    }
}
