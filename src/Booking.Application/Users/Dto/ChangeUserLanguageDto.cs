using System.ComponentModel.DataAnnotations;

namespace Booking.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}