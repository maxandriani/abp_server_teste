﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Booking.Booking;
using Booking.Booking.Dtos;
using Booking.Financial.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Financial.Contracts
{
    public interface ITransactionCrudService : IAsyncCrudAppService<TransactionDto, long, PagedAndSortedTransactionRequestDto, CreateTransactionDto, UpdateTransactionDto>
    {
        Task<TransactionDto> Confirm(ConfirmTransactionDto input);
        Task<TransactionDto> Cancel(EntityDto<long> input);
        Task<TransactionDto> Rollback(EntityDto<long> input);
        Task<PagedResultDto<TransactionDto>> GenerateFromBooking(GenerateTransactionFromBookingDto booking);
        Task<PagedResultDto<TransactionDto>> GenerateFromFee(GenerateTransactionFromFeeDto fee);
    }
}
