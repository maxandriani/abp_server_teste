﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Booking.Financial.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Financial.Contracts
{
    public interface IUserFeeCrudService : IAsyncCrudAppService<UserFeeDto, long, PagedAndSortedUserFeeRequestDto, CreateUserFeeDto, UpdateUserFeeDto, GetUserFeeDto, GetUserFeeDto>
    {
        Task<UserFeeDto> GetFull(GetUserFeeDto input);
    }
}
