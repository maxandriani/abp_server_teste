﻿using Abp.Application.Services;
using Booking.Financial.Dtos;
using System.Threading.Tasks;

namespace Booking.Financial.Contracts
{
    public interface IWalletCrudService : IAsyncCrudAppService<WalletDto, long, PagedAndSortedWalletRequestDto, CreateWalletDto, WalletDto>
    {
        Task<WalletDto> GetDefaultWallet();
    }
}
