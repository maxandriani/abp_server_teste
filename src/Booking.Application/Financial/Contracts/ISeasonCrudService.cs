﻿using Abp.Application.Services;
using Booking.Financial.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Contracts
{
    public interface ISeasonCrudService : IAsyncCrudAppService<SeasonDto, long, PagedAndSortedSeasonRequestDto, CreateSeasonDto, SeasonDto>
    {
    }
}
