﻿using Abp.Domain.Values;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class BalanceVo : ValueObject
    {
        public double Ammount { get; }

        public bool IsNegative
        {
            get
            {
                return (Ammount < 0);
            }
        }

        public bool IsPositive
        {
            get
            {
                return (Ammount > 0);
            }
        }
        
        public BalanceVo() { }

        public BalanceVo(double ammount) : this()
        {
            Ammount = ammount;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Ammount;
        }


    }
}
