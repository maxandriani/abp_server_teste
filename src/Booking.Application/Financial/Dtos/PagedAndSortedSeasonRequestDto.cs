﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class PagedAndSortedSeasonRequestDto : PagedAndSortedResultRequestDto
    {
        #region Search
        public string Search { get; set; }
        #endregion

        #region StartDate filters
        public DateTime? Start { get; set; }
        #endregion

        #region Finish DateFilter
        public DateTime? Finish { get; set; }
        #endregion
    }
}
