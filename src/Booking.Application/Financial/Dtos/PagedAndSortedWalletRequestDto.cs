﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class PagedAndSortedWalletRequestDto : PagedAndSortedResultRequestDto
    {
        #region Business properties
        public string Search { get; set; }
        public bool? isDefault { get; set; }
        #endregion
    }
}
