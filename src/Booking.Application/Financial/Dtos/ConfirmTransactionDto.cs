﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class ConfirmTransactionDto : EntityDto<long>
    {
        public DateTime ConfirmationAt { get; set; }
    }
}
