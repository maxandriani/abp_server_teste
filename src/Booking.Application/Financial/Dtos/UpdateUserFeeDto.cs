﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(UserFee))]
    public class UpdateUserFeeDto : EntityDto<long>
    {
        #region Business properties
        [Required]
        public double Percent { get; set; }

        #endregion

        #region Relational Properties
        [Required]
        public long UserId { get; set; }

        public long? PlaceId { get; set; }
        #endregion
    }
}
