﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Booking.Booking.Dtos;
using Booking.Places.Dto;
using Booking.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(Transaction))]
    public class TransactionDto : EntityDto<long>
    {
        #region Audit Properties
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }

        #endregion

        #region Business Properties
        [Required]
        public DateTime MatureAt { get; set; }

        public DateTime? ConfirmationAt { get; set; }

        [Required]
        public double Ammount { get; set; }

        public string Description { get; set; }
        #endregion

        #region Parent
        public long? ParentId { get; set; }
        #endregion

        #region Relational Properties
        public long? PlaceId { get; set; }

        public long? BookingId { get; set; }

        public UserDto User { get; set; }
        public long? UserId { get; set; }

        public SeasonDto Season { get; set; }
        [Required]
        public long SeasonId { get; set; }

        public WalletDto Wallet { get; set; }
        [Required]
        public long WalletId { get; set; }

        public UserFeeBookingDto Fee { get; set; }
        public long? FeeId { get; set; }
        #endregion
    }
}
