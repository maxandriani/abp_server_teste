﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(Transaction))]
    public class CreateTransactionDto
    {
        #region Business Properties
        [Required]
        public DateTime MatureAt { get; set; }

        public DateTime? ConfirmationAt { get; set; }

        public double Ammount { get; set; }

        [MaxLength(TransactionRules.MaxDescriptionLength)]
        public string Description { get; set; }
        #endregion

        #region Relational Properties
        public long? PlaceId { get; set; }

        public long? BookingId { get; set; }

        public long? UserId { get; set; }

        [Required]
        public long SeasonId { get; set; }

        [Required]
        public long WalletId { get; set; }

        public long? FeeId { get; set; }
        #endregion
    }
}
