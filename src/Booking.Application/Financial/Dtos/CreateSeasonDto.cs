﻿using Abp.AutoMapper;
using Booking.Booking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(Season))]
    public class CreateSeasonDto
    {
        #region Business Properties
        [Required]
        public DateTime Start { get; set; }

        [Required]
        public DateTime Finish { get; set; }

        [Required]
        [MaxLength(SeasonRules.MaxDescriptionLength)]
        public string Description { get; set; }
        #endregion
    }
}
