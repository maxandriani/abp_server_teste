﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class GetUserFeeDto : EntityDto<long>
    {
        public long? UserId { get; set; }
        
        public long? PlaceId { get; set; }
    }
}
