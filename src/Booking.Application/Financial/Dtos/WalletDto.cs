﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(Wallet))]
    public class WalletDto : EntityDto<long>
    {
        public bool? Default { get; set; }

        [Required]
        [MaxLength(WalletRules.MaxDescriptionLength)]
        public string Description { get; set; }
    }
}
