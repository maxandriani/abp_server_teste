﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class PagedAndSortedUserFeeRequestDto : PagedAndSortedResultRequestDto
    {
        #region Business properties

        public double? Percent { get; set; }

        #endregion

        #region Relational Properties
        public string UserName { get; set; }
        public long? UserId { get; set; }

        public string PlaceDescription { get; set; }
        public long? PlaceId { get; set; }
        public bool EmptyPlace { get; set; }
        #endregion
    }
}
