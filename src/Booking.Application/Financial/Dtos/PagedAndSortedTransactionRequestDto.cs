﻿using Booking.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class PagedAndSortedTransactionRequestDto : IAuditedRequestDto
    {

        #region Business Properties
        public DateTime? MatureAt { get; set; }
        public DateTime? MatureAtStart { get; set; }
        public DateTime? MatureAtFinish { get; set; }

        public DateTime? ConfirmationAt { get; set; }
        public DateTime? ConfirmationAtStart { get; set; }
        public DateTime? ConfirmationAtFinish { get; set; }

        public bool? ConfirmatedOnly { get; set; }
        public bool? NotConfirmatedOnly { get; set; }

        public double? Ammount { get; set; }
        public double? AmmountStart { get; set; }
        public double? AmmountFinish { get; set; }

        public string Description { get; set; }
        #endregion

        #region Parent
        public long? ParentId { get; set; }
        #endregion

        #region Relational Properties
        public long? PlaceId { get; set; }
        public long[] PlaceRange { get; set; }

        public long? BookingId { get; set; }
        public long[] BookingRange { get; set; }

        public long? UserId { get; set; }
        public long[] UserRange { get; set; }

        public long? SeasonId { get; set; }

        public long? WalletId { get; set; }

        public long? FeeId { get; set; }
        #endregion;

        #region Audited
        public long? CreatorUser { get; set; }
        public DateTime? CreationStart { get; set; }
        public DateTime? CreationEnd { get; set; }
        public DateTime? CreationTime { get; set; }
        public long? LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? LastModificationStart { get; set; }
        public DateTime? LastModificationEnd { get; set; }
        #endregion
    }
}
