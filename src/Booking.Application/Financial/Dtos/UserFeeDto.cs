﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Booking.Authorization.Users;
using Booking.Places;
using Booking.Places.Dto;
using Booking.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    [AutoMapTo(typeof(UserFee))]
    public class UserFeeDto : EntityDto<long>
    {
        #region Business properties

        [Required]
        public double Percent { get; set; }

        #endregion

        #region Relational Properties
        [Required]
        public UserDto User { get; set; }
        public long UserId { get; set; }

        public PlaceDto Place { get; set; }
        public long? PlaceId { get; set; }
        #endregion
    }
}
