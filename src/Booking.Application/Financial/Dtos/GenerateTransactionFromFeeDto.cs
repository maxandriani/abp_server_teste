﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Financial.Dtos
{
    public class GenerateTransactionFromFeeDto
    {
        [Required]
        public long FeeId { get; set; }
        [Required]
        public long WalletId { get; set; }
        [Required]
        public long SeasonId { get; set; }
    }
}
