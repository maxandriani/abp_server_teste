﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Financial
{
    public class WalletCrudConsts
    {
        #region Exceptions
        public const string YouCanNotRemoveDefaultWallet = "wallet.youCanNotRemoveDefaultWallet";
        #endregion
    }

    [AbpAuthorize]
    public class WalletCrudService : AsyncCrudAppService<Wallet, WalletDto, long, PagedAndSortedWalletRequestDto, CreateWalletDto, WalletDto>, IWalletCrudService
    {
        public WalletCrudService(IRepository<Wallet, long> repository) : base(repository)
        {
        }

        #region Filters
        protected override IQueryable<Wallet> CreateFilteredQuery(PagedAndSortedWalletRequestDto input)
        {
            var query = Repository.GetAll();

            query = ApplySearchFilter(query, input);
            query = ApplyIsDefaultFilter(query, input);

            return query;
        }

        protected IQueryable<Wallet> ApplySearchFilter(IQueryable<Wallet> query, PagedAndSortedWalletRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Search))
            {
                query = query.Where(w => w.Description.ToLower().Contains(input.Search.ToLower()));
            }

            return query;
        }

        protected IQueryable<Wallet> ApplyIsDefaultFilter(IQueryable<Wallet> query, PagedAndSortedWalletRequestDto input)
        {
            if (input.isDefault.HasValue)
            {
                query = query.Where(w => w.Default == input.isDefault.Value);
            }

            return query;
        }
        #endregion

        #region CRUD Overrides
        public override async Task<WalletDto> Create(CreateWalletDto input)
        {
            CheckCreatePermission();

            var wallet = MapToEntity(input);

            if (wallet.Default == true)
            {
                var defaultWallet = GetDefaultWalletQuery().First();

                if (defaultWallet != null)
                {
                    defaultWallet.Default = false;
                    await Repository.UpdateAsync(defaultWallet);
                }
            }

            await Repository.InsertAsync(wallet);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(wallet);
        }

        public override async Task<WalletDto> Update(WalletDto input)
        {
            CheckUpdatePermission();

            var wallet = ObjectMapper.Map<Wallet>(input);

            if (wallet.Default == true)
            {
                var defaultWallet = GetDefaultWalletQuery().First();

                if (defaultWallet != null)
                {
                    defaultWallet.Default = false;
                    await Repository.UpdateAsync(defaultWallet);
                }
            }

            await Repository.UpdateAsync(wallet);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(wallet);
        }

        public override Task Delete(EntityDto<long> input)
        {
            CheckDeletePermission();

            var wallet = Repository.Get(input.Id);

            CheckNotDefaultWallet(wallet);

            Repository.Delete(wallet.Id);

            return CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion

        #region BusinessRules
        protected void CheckNotDefaultWallet(Wallet wallet)
        {
            if (wallet.Default == true)
            {
                throw new UserFriendlyException((int) HttpStatusCode.PreconditionFailed, WalletCrudConsts.YouCanNotRemoveDefaultWallet);
            }
        }
        #endregion

        #region Query Methods
        protected IQueryable<Wallet> GetDefaultWalletQuery()
        {
            return Repository
                    .GetAll()
                    .Where(w => w.Default == true);
        }

        public async Task<WalletDto> GetDefaultWallet()
        {
            var wallet = GetDefaultWalletQuery()
                .FirstOrDefault();

            return MapToEntityDto(wallet);
        }
        #endregion;
    }
}
