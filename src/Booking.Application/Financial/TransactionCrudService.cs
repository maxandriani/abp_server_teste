﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Timing;
using Abp.UI;
using Booking.Booking;
using Booking.Booking.Dtos;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Booking.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Financial
{
    public static class TransactionBusiness
    {
        public const string ConfirmationDateCanNotBeLowerThanMatureDate = "transaction.confirmationDateCanNotBeLowerThanMatureDate";
        public const string CanNotDeleteConfirmedTransaction = "transaction.canNotDeleteConfirmedTransaction";
        public const string YouCanNotConfirmAConfirmedTransaction = "transaction.youCanNotConfirmAConfirmedTransaction";
        public const string YouCanNotRollbackAnUnconfirmedTransaction = "transaction.youCanNotRollbackAnUnconfirmedTransaction";
        public const string ThisTransactionIsAlreadyRolback = "transaction.thisTransactionIsAlreadyRollback";
        public const string ThisBookingAlreadyHasTransactions = "transaction.thisBookingAlreadyHasTransactions";
        public const string ThisFeeAlreadyHasTransactions = "transaction.thisFeeAlreadyHasTransactions";
    }

    [AbpAuthorize]
    public class TransactionCrudService : AsyncCrudAppService<Transaction, TransactionDto, long, PagedAndSortedTransactionRequestDto, CreateTransactionDto, UpdateTransactionDto>, ITransactionCrudService
    {
        protected readonly IAuditedQueryParserService ApplyAudited;
        protected readonly IRepository<Booking.Booking, long> Bookings;
        protected readonly IRepository<Booking.UserFeeBooking, long> Fees;

        public TransactionCrudService(
            IRepository<Transaction, long> repository,
            IAuditedQueryParserService applyAudited,
            IRepository<Booking.Booking, long> bookings,
            IRepository<Booking.UserFeeBooking, long> fees
        ) : base(repository)
        {
            ApplyAudited = applyAudited;
            Bookings = bookings;
            Fees = fees;
        }

        #region Business Rules
        protected async Task CheckBookingTransactionAlreadyGenerated(Booking.Booking b)
        {
            if (b.Transactions.Count() > 0)
            {
                throw new UserFriendlyException(TransactionBusiness.ThisBookingAlreadyHasTransactions);
            }
        }
        protected async Task CheckFeeTransactionAlreadyGenerated(Booking.UserFeeBooking ufb)
        {
            if (ufb.Transactions.Count() > 0)
            {
                throw new UserFriendlyException(TransactionBusiness.ThisFeeAlreadyHasTransactions);
            }
        }
        protected async Task CheckDates(Transaction t) {
            if (t.ConfirmationAt.HasValue && DateTime.Compare(t.MatureAt, t.ConfirmationAt.Value) > 0)
            {
                throw new UserFriendlyException(TransactionBusiness.ConfirmationDateCanNotBeLowerThanMatureDate);
            }
        }
        protected async Task CheckCanDelete(Transaction t)
        {
            if (t.ConfirmationAt.HasValue)
            {
                throw new UserFriendlyException(TransactionBusiness.CanNotDeleteConfirmedTransaction);
            }
        }
        protected async Task CheckCanRollback(Transaction t)
        {
            if (!t.Parent.ConfirmationAt.HasValue)
            {
                throw new UserFriendlyException(TransactionBusiness.YouCanNotRollbackAnUnconfirmedTransaction);
            }
        }
        protected async Task CheckDuplicatedRollback(Transaction t)
        {
            var rollback = Repository
                .GetAll()
                .Where(r => r.ParentId == t.ParentId)
                .Count();

            if (rollback > 1)
            {
                throw new UserFriendlyException(TransactionBusiness.ThisTransactionIsAlreadyRolback);
            }
        }
        protected async Task AssureBookingIntegrity(Transaction t) {
            if (t.BookingId.HasValue)
            {
                var booking = await Bookings.GetAsync(t.BookingId.Value);
                t.PlaceId = booking.PlaceId;
            }
        }
        protected async Task AssureFeeIntegrity(Transaction t) {
            if (t.FeeId.HasValue)
            {
                var fee = await Fees.GetAsync(t.FeeId.Value);
                t.UserId = fee.Fee.UserId;
                t.BookingId = fee.BookingId;
                t.PlaceId = fee.Booking.PlaceId;
            }
        }
        #endregion

        #region Queries
        protected override IQueryable<Transaction> CreateFilteredQuery(PagedAndSortedTransactionRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            query = ApplyAudited.Parse(query, input).Cast<Transaction>();
            query = ApplyMatureFilters(query, input);
            query = ApplyConfirmationFilters(query, input);
            query = ApplyAmmountFilters(query, input);
            query = ApplyDescriptionFilters(query, input);
            query = ApplyParentFilter(query, input);
            query = ApplyPlaceFilters(query, input);
            query = ApplyBookingFilters(query, input);
            query = ApplyUserFilters(query, input);
            query = ApplyTransactionFilters(query, input);
            query = ApplyWalletFilters(query, input);
            query = ApplyFeeFilters(query, input);

            return query;
        }

        // MatureAt
        protected IQueryable<Transaction> ApplyMatureFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.MatureAt.HasValue)
            {
                input.MatureAtStart = new DateTime(
                    input.MatureAt.Value.Year,
                    input.MatureAt.Value.Month,
                    input.MatureAt.Value.Day,
                    0, 0, 0);

                input.MatureAtFinish = input.MatureAtStart.Value.AddDays(1);
            }

            if (input.MatureAtStart.HasValue)
            {
                query = query.Where(t => t.MatureAt >= input.MatureAtStart);
            }

            if (input.MatureAtFinish.HasValue)
            {
                query = query.Where(t => t.MatureAt < input.MatureAtFinish);
            }

            return query;
        }
        
        // Confirmation At
        protected IQueryable<Transaction> ApplyConfirmationFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.ConfirmationAt.HasValue)
            {
                input.ConfirmationAtStart = new DateTime(
                    input.ConfirmationAt.Value.Year,
                    input.ConfirmationAt.Value.Month,
                    input.ConfirmationAt.Value.Day,
                    0, 0, 0);

                input.ConfirmationAtFinish = input.ConfirmationAtStart.Value.AddDays(1);
            }

            if (input.ConfirmationAtStart.HasValue)
            {
                query = query.Where(t => t.ConfirmationAt >= input.ConfirmationAtStart);
            }

            if (input.ConfirmationAtFinish.HasValue)
            {
                query = query.Where(t => t.ConfirmationAt <= input.ConfirmationAtFinish);
            }

            if (input.ConfirmatedOnly.HasValue)
            {
                query = query.Where(t => t.ConfirmationAt != null);
            }

            if (input.NotConfirmatedOnly.HasValue)
            {
                query = query.Where(t => t.ConfirmationAt == null);
            }

            return query;
        }

        // Ammount Filters
        protected IQueryable<Transaction> ApplyAmmountFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.Ammount.HasValue)
            {
                query = query.Where(t => t.Ammount == input.Ammount);
            }

            if (input.AmmountStart.HasValue)
            {
                query = query.Where(t => t.Ammount >= input.AmmountStart);
            }

            if (input.AmmountFinish.HasValue)
            {
                query = query.Where(t => t.Ammount < input.AmmountFinish);
            }

            return query;
        }
        
        // Lexal Filter
        protected IQueryable<Transaction> ApplyDescriptionFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (!String.IsNullOrWhiteSpace(input.Description))
            {
                query = query.Where(t => t.Description.ToLower().Contains(input.Description.ToLower()));
            }

            return query;
        }

        // Children
        protected IQueryable<Transaction> ApplyParentFilter(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.ParentId.HasValue)
            {
                query = query.Where(t => t.ParentId == input.ParentId);
            }

            return query;
        }
        
        // Place Filters
        protected IQueryable<Transaction> ApplyPlaceFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.PlaceId.HasValue)
            {
                query = query.Where(t => t.PlaceId == input.PlaceId);
            }

            if (input.PlaceRange != null && input.PlaceRange.Length > 0)
            {
                query = query.Where(t => input.PlaceRange.Contains(t.PlaceId.Value));
            }

            return query;
        }
        
        // Booking Filters
        protected IQueryable<Transaction> ApplyBookingFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.BookingId.HasValue)
            {
                query = query.Where(t => t.BookingId == input.BookingId);
            }

            if (input.BookingRange != null && input.BookingRange.Length > 0)
            {
                query = query.Where(t => input.BookingRange.Contains(t.BookingId.Value));
            }

            return query;
        }

        // User Filters
        protected IQueryable<Transaction> ApplyUserFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.UserId.HasValue)
            {
                query = query.Where(t => t.UserId == input.UserId);
            }

            if (input.UserRange != null && input.UserRange.Length > 0)
            {
                query = query.Where(t => input.UserRange.Contains(t.UserId.Value));
            }

            return query;
        }

        // Season Filters
        protected IQueryable<Transaction> ApplyTransactionFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.SeasonId.HasValue)
            {
                query = query.Where(t => t.SeasonId == input.SeasonId);
            }

            return query;
        }

        // Wallet Filters
        protected IQueryable<Transaction> ApplyWalletFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.WalletId.HasValue)
            {
                query = query.Where(t => t.WalletId == input.WalletId);
            }

            return query;
        }

        // Fee Filters
        protected IQueryable<Transaction> ApplyFeeFilters(IQueryable<Transaction> query, PagedAndSortedTransactionRequestDto input)
        {
            if (input.FeeId.HasValue)
            {
                query = query.Where(t => t.FeeId == input.FeeId);
            }

            return query;
        }
        #endregion

        #region Override CRUD
        public override async Task<TransactionDto> Create(CreateTransactionDto input)
        {
            CheckCreatePermission();

            var t = MapToEntity(input);

            // Temporary fil nullable auto mapper
            t.ConfirmationAt = input.ConfirmationAt;

            await CheckDates(t);
            await AssureBookingIntegrity(t);
            await AssureFeeIntegrity(t);

            await Repository.InsertAndGetIdAsync(t);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(t);
        }

        public override async Task<TransactionDto> Update(UpdateTransactionDto input)
        {
            CheckDeletePermission();

            var t = await Repository.GetAsync(input.Id);

            if (t.ConfirmationAt.HasValue)
            {
                t.Description = input.Description;
            } else
            {
                t = ObjectMapper.Map<UpdateTransactionDto, Transaction>(input, t);
                // Temporary fix
                t.ConfirmationAt = input.ConfirmationAt;
            }

            await CheckDates(t);
            await AssureBookingIntegrity(t);
            await AssureFeeIntegrity(t);

            await Repository.UpdateAsync(t);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(t);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            CheckDeletePermission();

            var t = await Repository.GetAsync(input.Id);

            await CheckCanDelete(t);
            await Repository.DeleteAsync(t);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<TransactionDto> Confirm(ConfirmTransactionDto input)
        {
            CheckUpdatePermission();

            var t = await Repository.GetAsync(input.Id);

            t.ConfirmationAt = input.ConfirmationAt;

            await CheckDates(t);
            await Repository.UpdateAsync(t);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(t);
        }

        public async Task<TransactionDto> Cancel(EntityDto<long> input)
        {
            CheckUpdatePermission();

            var t = await Repository.GetAsync(input.Id);
            t.ConfirmationAt = null;
            await Repository.UpdateAsync(t);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(t);
        }

        public async Task<PagedResultDto<TransactionDto>> GenerateFromBooking(GenerateTransactionFromBookingDto input)
        {
            CheckCreatePermission();

            var b = await Bookings.GetAsync(input.BookingId);

            await CheckBookingTransactionAlreadyGenerated(b);

            var ammount = (b.PaymentSplit > 1)
                ? Math.Round((b.Ammount / b.PaymentSplit), 2)
                : b.Ammount;

            var matureAt = b.Start.AddMonths( (b.PaymentSplit - 1) * -1 );
            var transactions = new List<Transaction>();

            for (ushort split = 1; split <= b.PaymentSplit; split++) {
                var t = new Transaction
                {
                    BookingId = b.Id,
                    PlaceId = b.PlaceId,
                    WalletId = input.WalletId,
                    SeasonId = input.SeasonId,
                    Ammount = (split == 1 && b.PaymentSplit > 1)
                        ? (b.Ammount - (b.Ammount / (b.PaymentSplit -1)))
                        : ammount,
                    Description = String.Format("[booking: {0}] {1}. ({2}/{3})", b.Id, b.Name, split, b.PaymentSplit),
                    MatureAt = matureAt
                };

                // Save
                await Repository.InsertAndGetIdAsync(t);
                transactions.Add(t);

                // Generate Fees
                foreach (var fee in b.Fees)
                {
                    var tf = new Transaction
                    {
                        BookingId = b.Id,
                        PlaceId = b.PlaceId,
                        WalletId = input.WalletId,
                        SeasonId = input.SeasonId,
                        FeeId = fee.Id,
                        Ammount = fee.Ammount / b.PaymentSplit,
                        UserId = fee.Fee.UserId,
                        MatureAt = matureAt,
                        Description = String.Format("[booking: {0}] [fee: {1}] {2}. ({3}/{4})", b.Id, fee.Id, fee.User.Name, split, b.PaymentSplit)
                    };

                    await Repository.InsertAndGetIdAsync(tf);
                    transactions.Add(tf);
                }

                // Update Dates
                matureAt = matureAt.AddMonths(1);
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            return new PagedResultDto<TransactionDto>(
                transactions.Count(),
                ObjectMapper.Map<IReadOnlyList<TransactionDto>>(transactions)
            );
        }

        public async Task<PagedResultDto<TransactionDto>> GenerateFromFee(GenerateTransactionFromFeeDto input)
        {
            CheckCreatePermission();

            var fee = await Fees.GetAsync(input.FeeId);

            await CheckFeeTransactionAlreadyGenerated(fee);

            var ammount = fee.Ammount / fee.Booking.PaymentSplit;
            var matureAt = fee.Booking.Start.AddMonths((fee.Booking.PaymentSplit - 1) * -1);
            var transactions = new List<Transaction>();

            for (ushort split = 1; split <= fee.Booking.PaymentSplit; split ++)
            {
                var t = new Transaction
                {
                    BookingId = fee.Booking.Id,
                    PlaceId = fee.Booking.PlaceId,
                    WalletId = input.WalletId,
                    SeasonId = input.SeasonId,
                    FeeId = fee.Id,
                    Ammount = ammount,
                    UserId = fee.Fee.UserId,
                    MatureAt = matureAt,
                    Description = String.Format("[booking: {0}] [fee: {1}] {2}. ({3}/{4})", fee.Booking.Id, fee.Id, fee.User.Name, split, fee.Booking.PaymentSplit)
                };

                await Repository.InsertAndGetIdAsync(t);
                transactions.Add(t);
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            return new PagedResultDto<TransactionDto>(
                transactions.Count(),
                ObjectMapper.Map<IReadOnlyList<TransactionDto>>(transactions)
            );
        }

        public async Task<TransactionDto> Rollback(EntityDto<long> input)
        {
            CheckCreatePermission();
            CheckUpdatePermission();

            var t = await Repository.GetAsync(input.Id);
            var rollbackDto = ObjectMapper.Map<TransactionDto>(t);
            var rollback = ObjectMapper.Map<Transaction>(rollbackDto);

            rollback.Id = 0;
            rollback.Parent = t;
            rollback.ParentId = t.Id;
            rollback.ConfirmationAt = Clock.Now;
            rollback.Ammount = rollback.Ammount * -1;

            await CheckCanRollback(rollback);
            await CheckDuplicatedRollback(rollback);

            await Repository.InsertAsync(rollback);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(rollback);
        }
        #endregion
    }
}
