﻿

using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using Booking.Authorization.Users;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Booking.Places;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Booking.Financial
{
    public static class UserFeeBusinesErrors
    {
        public const string YouCanOnlyHaveOneFeePerUserAndPlace = "userFee.youCanOnlyHaveOneFeePerUserAndPlace";
        public const string YouCanOnlyHaveOneGeneralFeePerUser = "userFee.youCanOnlyHaveOneGeneralFeePerUser";
    }

    [AbpAuthorize]
    public class UserFeeCrudService : AsyncCrudAppService<UserFee, UserFeeDto, long, PagedAndSortedUserFeeRequestDto, CreateUserFeeDto, UpdateUserFeeDto, GetUserFeeDto, GetUserFeeDto>, IUserFeeCrudService
    {

        protected readonly IRepository<User, long> Users;
        protected readonly IRepository<Place, long> Places;

        public UserFeeCrudService(
            IRepository<UserFee, long> repository,
            IRepository<User, long> users,
            IRepository<Place, long> places
        ) : base(repository)
        {
            Users = users;
            Places = places;
        }

        #region Business Validations
        protected void CheckUniqueFeePerUserAndPlace(UserFee fee)
        {
            if (fee.PlaceId != 0 && fee.UserId != 0)
            {
                var duplicated = CreateFilteredQuery(new PagedAndSortedUserFeeRequestDto { PlaceId = fee.PlaceId, UserId = fee.UserId })
                    .FirstOrDefault();

                if (duplicated != null)
                {
                    throw new UserFriendlyException((int)HttpStatusCode.BadRequest, UserFeeBusinesErrors.YouCanOnlyHaveOneFeePerUserAndPlace);
                }
            }
        }

        protected void CheckUniqueFeePerUser(UserFee fee)
        {
            if (fee.UserId != 0 && fee.PlaceId == 0)
            {
                var duplicated = CreateFilteredQuery(new PagedAndSortedUserFeeRequestDto
                {
                    UserId = fee.UserId,
                    EmptyPlace = true
                })
                .FirstOrDefault();

                if (duplicated != null)
                {
                    throw new UserFriendlyException((int)HttpStatusCode.BadRequest, UserFeeBusinesErrors.YouCanOnlyHaveOneGeneralFeePerUser);
                }
            }
        }
        #endregion

        #region Filters

        protected IQueryable<UserFee> ApplyPercentFilter(IQueryable<UserFee> query, PagedAndSortedUserFeeRequestDto input)
        {
            if (input.Percent.HasValue)
            {
                query = query.Where(uf => uf.Percent == input.Percent);
            }

            return query;
        }

        protected IQueryable<UserFee> ApplyUserFilter(IQueryable<UserFee> query, PagedAndSortedUserFeeRequestDto input)
        {
            if (input.UserId.HasValue)
            {
                query = query.Where(uf => uf.UserId == input.UserId);
            }

            if (!String.IsNullOrEmpty(input.UserName))
            {
                query = query.Where(
                    uf => uf.User.Name.Contains(input.UserName) || uf.User.UserName.Contains(input.UserName)
                );
            }

            return query;
        }

        protected IQueryable<UserFee> ApplyPlaceFilter(IQueryable<UserFee> query, PagedAndSortedUserFeeRequestDto input)
        {
            if (input.PlaceId.HasValue)
            {
                query = query.Where(uf => uf.PlaceId == input.PlaceId);
            }

            if (input.EmptyPlace)
            {
                query = query.Where(uf => uf.PlaceId == null);
            }

            if (!String.IsNullOrEmpty(input.PlaceDescription))
            {
                query = query.Where(uf => uf.Place.Description.Contains(input.PlaceDescription));
            }

            return query;
        }
        
        protected override IQueryable<UserFee> CreateFilteredQuery(PagedAndSortedUserFeeRequestDto input)
        {
            var query = Repository.GetAll();

            query = ApplyPercentFilter(query, input);
            query = ApplyPlaceFilter(query, input);
            query = ApplyUserFilter(query, input);

            return query;
        }
        #endregion

        #region CRUD overrides
        public override async Task<UserFeeDto> Create(CreateUserFeeDto input)
        {
            var userFee = MapToEntity(input);
            var user = await Users.GetAsync(input.UserId);
            userFee.User = user;

            if (input.PlaceId.HasValue)
            {
                var place = await Places.GetAsync(input.PlaceId.Value);
                userFee.Place = place;
            }

            CheckCreatePermission();
            CheckUniqueFeePerUser(userFee);
            CheckUniqueFeePerUserAndPlace(userFee);

            await Repository.InsertAsync(userFee);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(userFee);
        }

        public override async Task<UserFeeDto> Update(UpdateUserFeeDto input)
        {
            var userFee = await Repository.GetAsync(input.Id);
            userFee = ObjectMapper.Map<UpdateUserFeeDto, UserFee>(input, userFee);
            var user = await Users.GetAsync(input.UserId);
            userFee.User = user;

            if (input.PlaceId.HasValue)
            {
                var place = await Places.GetAsync(input.PlaceId.Value);
                userFee.Place = place;
            }

            CheckUpdatePermission();
            CheckUniqueFeePerUser(userFee);
            CheckUniqueFeePerUserAndPlace(userFee);

            await Repository.UpdateAsync(userFee);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(userFee);
        }
        #endregion

        #region Queries
        public async Task<UserFeeDto> GetFull(GetUserFeeDto input)
        {
            CheckGetPermission();

            var ufQuery = Repository
                .GetAll()
                .Where(uf => uf.Id == input.Id);

            if (input.UserId.HasValue)
            {
                ufQuery = ufQuery.Where(uf => uf.UserId == input.UserId);
            }
            
            if (input.PlaceId.HasValue)
            {
                ufQuery = ufQuery.Where(uf => uf.PlaceId == input.PlaceId);
            }

            var userFee = ufQuery.First();

            return MapToEntityDto(userFee);
        }

        public override async Task<UserFeeDto> Get(GetUserFeeDto input)
        {
            CheckGetPermission();

            var feeQuery = Repository
                .GetAll()
                .Where(uf => uf.Id == input.Id);

            if (input.UserId.HasValue)
            {
                feeQuery = feeQuery.Where(f => f.UserId == input.UserId);
            }

            if (input.PlaceId.HasValue)
            {
                feeQuery = feeQuery.Where(uf => uf.PlaceId == input.PlaceId);
            }

            return MapToEntityDto(feeQuery.First());
        }

        public override async Task Delete(GetUserFeeDto input)
        {
            CheckDeletePermission();

            var feeQ = Repository
                .GetAll()
                .Where(f => f.Id == input.Id);

            if (input.PlaceId.HasValue)
            {
                feeQ = feeQ.Where(f => f.PlaceId == input.PlaceId);
            }

            if (input.UserId.HasValue)
            {
                feeQ = feeQ.Where(f => f.UserId == input.UserId);
            }

            await Repository.DeleteAsync(feeQ.First());
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion

    }
}
