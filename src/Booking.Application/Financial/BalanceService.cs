﻿using Abp.Authorization;
using Booking.Financial.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Financial
{
    [AbpAuthorize]
    public class BalanceService : BookingAppServiceBase, IBalanceService
    {
    }
}
