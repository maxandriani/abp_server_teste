﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using Booking.Booking;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Financial
{
    public static class SeasonBusinessErrors {
        public const string StartDateMustBeLowerThenFinishDate = "season.startDateMustBeLowerThanFinishDate";
    }

    [AbpAuthorize]
    public class SeasonCrudService : AsyncCrudAppService<Season, SeasonDto, long, PagedAndSortedSeasonRequestDto, CreateSeasonDto, SeasonDto>, ISeasonCrudService
    {
        public SeasonCrudService(IRepository<Season, long> repository) : base(repository)
        {
        }

        #region Filters
        protected virtual IQueryable<Season> ApplyDateFilters(IQueryable<Season> query, PagedAndSortedSeasonRequestDto input)
        {
            if (input.Start.HasValue)
            {
                input.Start = new DateTime(
                    input.Start.Value.Year,
                    input.Start.Value.Month,
                    input.Start.Value.Day,
                    0,0,0
                );

                query = query.Where(s => s.Start >= input.Start && s.Finish >= input.Start);
            }

            if (input.Finish.HasValue)
            {
                input.Finish = new DateTime(
                    input.Finish.Value.Year,
                    input.Finish.Value.Month,
                    input.Finish.Value.Day,
                    23, 59, 59
                );

                query = query.Where(s => s.Finish <= input.Finish && s.Start >= s.Finish);
            }

            return query;
        }

        protected virtual IQueryable<Season> ApplySearchFilter(IQueryable<Season> query, PagedAndSortedSeasonRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Search))
            {
                query = query.Where(s => s.Description.ToLower().Contains(input.Search.ToLower()));
            }

            return query;
        }

        protected override IQueryable<Season> CreateFilteredQuery(PagedAndSortedSeasonRequestDto input)
        {
            var query = Repository.GetAll();

            query = ApplySearchFilter(query, input);
            query = ApplyDateFilters(query, input);

            return query;
        }
        #endregion

        #region Business Rules
        protected void CheckStartFinishDate(Season season)
        {
            if (DateTime.Compare(season.Start, season.Finish) >= 0)
            {
                throw new UserFriendlyException((int)HttpStatusCode.BadRequest, SeasonBusinessErrors.StartDateMustBeLowerThenFinishDate);
            }
        }
        #endregion

        #region CRUD Overrides
        public override Task<SeasonDto> Create(CreateSeasonDto input)
        {
            CheckUpdatePermission();
            CheckStartFinishDate(ObjectMapper.Map<Season>(input));

            return base.Create(input);
        }

        public override Task<SeasonDto> Update(SeasonDto input)
        {
            CheckUpdatePermission();
            CheckStartFinishDate(ObjectMapper.Map<Season>(input));

            return base.Update(input);
        }
        #endregion

        #region Business Methods
        #endregion
    }
}
