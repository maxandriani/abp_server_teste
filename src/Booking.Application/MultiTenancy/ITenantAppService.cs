﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Booking.MultiTenancy.Dto;

namespace Booking.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

