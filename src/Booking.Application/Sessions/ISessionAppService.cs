﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Booking.Sessions.Dto;

namespace Booking.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
