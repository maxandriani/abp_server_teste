﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Booking.Financial.Dtos;
using Booking.Places.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(Booking))]
    public class BookingDto : EntityDto<long>
    {
        #region Audition Properties
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        #endregion

        #region Business Properties
        [Required]
        public DateTime Start { get; set; }

        [Required]
        public DateTime Finish { get; set; }

        [MaxLength(BookingRules.MaxNameLength)]
        public string Name { get; set; }

        [MaxLength(BookingRules.MaxEmailLength)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public BookingStatus Status { get; set; }

        [Required]
        public double Ammount { get; set; }
        public ushort PaymentSplit { get; set; } = 1;
        #endregion

        #region Relational Properties
        public PlaceDto Place { get; set; }
        [Required]
        public long PlaceId { get; set; }

        public SourceDto Source { get; set; }
        public long? SourceId { get; set; } /// @TODO Configure onDelete=SET NULL

        public ICollection<BookingPhoneDto> Phones { get; set; }

        public ICollection<TransactionDto> Transactions { get; set; }

        public ICollection<UserFeeBookingDto> Fees { get; set; }
        #endregion
    }
}
