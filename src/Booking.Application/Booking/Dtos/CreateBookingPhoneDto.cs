﻿using Abp.AutoMapper;
using Booking.CommonVO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(BookingPhone))]
    public class CreateBookingPhoneDto
    {
        #region Business Properties
        [Required]
        public Phone Phone { get; set; }
        #endregion

        #region Relational Properties
        [Required]
        public long BookingId { get; set; }
        #endregion
    }
}
