﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(UserFeeBooking))]
    public class GetUserFeeBookingDto : EntityDto<long>
    {
        #region Relational prperties
        [Required]
        public long UserId { get; set; }

        [Required]
        public long BookingId { get; set; }
        #endregion
    }
}
