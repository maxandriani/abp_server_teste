﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Dtos
{
    public class SearchAvailableBookingDatesDto
    {
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public long? PlaceId { get; set; }
    }
}
