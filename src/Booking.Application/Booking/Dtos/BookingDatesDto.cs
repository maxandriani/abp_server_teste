﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(PagedAndSortedBookingRequestDto))]
    public class BookingDatesDto : EntityDto<long>
    {
        public DateTime? Start { get; set; }

        public DateTime? Finish { get; set; }
    }
}
