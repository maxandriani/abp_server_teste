﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(Booking))]
    public class UpdateBookingDto : EntityDto<long>
    {
        #region Business Properties
        [MaxLength(BookingRules.MaxNameLength)]
        public string Name { get; set; }

        [MaxLength(BookingRules.MaxEmailLength)]
        [EmailAddress]
        public string Email { get; set; }

        public double Ammount { get; set; }
        public ushort PaymentSplit { get; set; } = 1;
        #endregion

        public long? SourceId { get; set; } /// @TODO Configure onDelete=SET NULL
    }
}
