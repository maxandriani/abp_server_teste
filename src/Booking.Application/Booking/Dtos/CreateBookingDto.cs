﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(Booking))]
    public class CreateBookingDto
    {
        #region Business Properties
        [Required]
        public DateTime Start { get; set; }

        [Required]
        public DateTime Finish { get; set; }

        [MaxLength(BookingRules.MaxNameLength)]
        public string Name { get; set; }

        [MaxLength(BookingRules.MaxEmailLength)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public BookingStatus Status { get; set; } = BookingStatus.Waiting;

        [Required]
        public double? Ammount { get; set; }
        public ushort PaymentSplit { get; set; } = 1;
        #endregion

        #region Relational Properties
        [Required]
        public long PlaceId { get; set; }

        public long? SourceId { get; set; } /// @TODO Configure onDelete=SET NULL

        public ICollection<CreateUserFeeBookingDto> Fees { get; set; }

        public ICollection<CreateBookingPhoneDto> Phones { get; set; }
        #endregion

        public CreateBookingDto()
        {
            Fees = new List<CreateUserFeeBookingDto>();
            Phones = new List<CreateBookingPhoneDto>();
        }
    }
}
