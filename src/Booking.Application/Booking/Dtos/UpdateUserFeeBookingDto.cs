﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(UserFeeBooking))]
    public class UpdateUserFeeBookingDto : EntityDto<long>
    {
        #region Business properties
        public double? Ammount { get; set; }
        #endregion

        #region Relational prperties
        public long? UserId { get; set; }

        public long? FeeId { get; set; }

        public long BookingId { get; set; }
        #endregion
    }
}
