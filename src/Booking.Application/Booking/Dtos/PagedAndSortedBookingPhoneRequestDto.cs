﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Dtos
{
    public class PagedAndSortedBookingPhoneRequestDto : PagedAndSortedResultRequestDto
    {
        public string Search { get; set; }

        #region Business Properties
        public ushort? Ddi { get; set; }
        public ushort? Ddd { get; set; }
        public int? Number { get; set; }
        #endregion

        #region Relational Properties
        public long? BookingId { get; set; }
        #endregion
    }
}
