﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Booking.Financial.Dtos;
using Booking.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    [AutoMapTo(typeof(UserFeeBooking))]
    public class UserFeeBookingDto : EntityDto<long>
    {
        #region Business properties
        [Required]
        public double Ammount { get; set; }
        #endregion

        #region Relational prperties
        public long UserId { get; set; }

        public long? FeeId { get; set; }

        public long BookingId { get; set; }
        #endregion
    }
}
