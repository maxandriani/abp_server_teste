﻿using Abp.Application.Services.Dto;
using Booking.CommonVO;
using Booking.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Booking.Dtos
{
    public class PagedAndSortedBookingRequestDto : PagedAndSortedResultRequestDto, IAuditedRequestDto
    {

        #region Audition Properties
        public long? CreatorUser { get; set; }
        public DateTime? CreationStart { get; set; }
        public DateTime? CreationEnd { get; set; }
        public DateTime? CreationTime { get; set; }

        public long? LastModifierUser { get; set; }
        public DateTime? LastModificationStart { get; set; }
        public DateTime? LastModificationEnd { get; set; }
        public DateTime? LastModificationTime { get; set; }
        #endregion

        #region Business Properties
        public DateTime? Start { get; set; }
        public DateTime? Finish { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public BookingStatus? Status { get; set; }
        #endregion

        #region Relational Properties
        public long? PlaceId { get; set; }
        public string PlaceSearch { get; set; }

        public long? SourceId { get; set; }
        public string SourceSearch { get; set; }

        public long? PhoneId { get; set; }
        public Phone Phone { get; set; }

        public long? TransactionId { get; set; }

        public DateTime? TransactionDate { get; set; }

        public double? TransactionValue { get; set; }

        public long? UserId { get; set; }
        public string UserSearch { get; set; }
        #endregion
    }
}
