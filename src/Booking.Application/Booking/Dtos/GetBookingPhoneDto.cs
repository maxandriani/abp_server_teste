﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Dtos
{
    public class GetBookingPhoneDto : EntityDto<long>
    {
        public long BookingId { get; set; }
    }
}
