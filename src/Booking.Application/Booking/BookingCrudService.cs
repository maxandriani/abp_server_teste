﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Repositories;
using Abp.UI;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using Booking.CommonVO;
using Booking.Financial;
using Booking.Financial.Dtos;
using Booking.Places;
using Booking.Places.Dto;
using Booking.Utils;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Booking
{
    public static class BookingBusiness
    {
        public const string ThisBookingAlreadyHaveConfirmedTransactions = "booking.thisBookinAlreadyHaveConfirmedTransactions";
        public const string ThereAreBookingConflictsToChoosenDates = "booking.thereAreBookingConflictsToChoosenDates";
        public const string DatesCantBeEmpty = "booking.datesCantBeEmpty";
        public const string FirstDateShallBeLowerThanFinishDate = "booking.firstDateShallBeLowerThanFinishDate";
        public const string YouCanNotRegisterABookingWithOutPlace = "booking.youCanNotRegisterABookingWithOutPlace";
        public const string YouCanNotRegisterANegativeAmmount = "booking.youCanNotRegisterANegativeAmmount";
        public const string YouCanNotRegisterLessThenOnePayment = "booking.youCanNotRegisterLessThenOnePayment";
    }

    [AbpAuthorize]
    public class BookingCrudService : AsyncCrudAppService<Booking, BookingDto, long, PagedAndSortedBookingRequestDto, CreateBookingDto, UpdateBookingDto, EntityDto<long>, EntityDto<long>>, IBookingCrudService
    {
        protected readonly IAuditedQueryParserService AuditedQueryParser;
        protected readonly IRepository<UserFeeBooking, long> UserFeeBooking;
        protected readonly IRepository<UserFee, long> UserFee;
        protected readonly IRepository<Transaction, long> Transaction;
        protected readonly IRepository<Place, long> Place;
        public BookingCrudService(
            IRepository<Booking, long> repository,
            IAuditedQueryParserService auditedQueryParser,
            IRepository<UserFeeBooking, long> userFeeBooking,
            IRepository<UserFee, long> userFee,
            IRepository<Transaction, long> transaction,
            IRepository<Place, long> place
        ) : base(repository)
        {
            AuditedQueryParser = auditedQueryParser;
            UserFeeBooking = userFeeBooking;
            UserFee = userFee;
            Transaction = transaction;
            Place = place;
        }

        #region Custom Queries
        protected IQueryable<Booking> CreateQueryAvailableDates(SearchAvailableBookingDatesDto input)
        {
            var query = Repository.GetAll();

            input.Start = new DateTime(
                input.Start.Year,
                input.Start.Month,
                input.Start.Day,
                0, 0, 0);

            query = query.Where(b => b.Finish >= input.Start);

            input.Finish = new DateTime(
                input.Finish.Year,
                input.Finish.Month,
                input.Finish.Day,
                23, 59, 59);

            query = query.Where(b => b.Start <= input.Finish);
            

            if (input.PlaceId.HasValue)
            {
                query = query.Where(b => b.PlaceId == input.PlaceId);
            }

            return query;
        }
        #endregion

        #region Business Checks
        // Check Payment Split and Value
        protected async Task CheckFinancialValues(Booking booking)
        {
            if (booking.Ammount < 0)
            {
                throw new UserFriendlyException(BookingBusiness.YouCanNotRegisterANegativeAmmount);
            }

            if (booking.PaymentSplit < 1)
            {
                throw new UserFriendlyException(BookingBusiness.YouCanNotRegisterLessThenOnePayment);
            }
        }
        // Check Has Confirmed Transactions
        protected async Task CheckHasConfirmedTransactionsAsync(Booking booking)
        {
            var transactionsCount = booking
                .Transactions
                .Where(t => t.ConfirmationAt != null)
                .Count();

            if (transactionsCount > 0)
            {
                throw new NotImplementedException(BookingBusiness.ThisBookingAlreadyHaveConfirmedTransactions);
            }
        }

        // Check booking dates
        protected async Task CheckBookingAvailableDates(Booking booking)
        {
            var isAvailable = await IsAvalidable(new SearchAvailableBookingDatesDto
            {
                PlaceId = booking.PlaceId,
                Start = new DateTime(
                    booking.Start.Year,
                    booking.Start.Month,
                    booking.Start.Day,
                    0, 0, 0),
                Finish = new DateTime(
                    booking.Finish.Year,
                    booking.Finish.Month,
                    booking.Finish.Day,
                    23, 59, 59)
            });

            if (!isAvailable)
            {
                throw new UserFriendlyException(BookingBusiness.ThereAreBookingConflictsToChoosenDates);
            }
        }

        protected async Task CheckBookingDates(Booking booking)
        {
            if (booking.Start == null || booking.Finish == null)
            {
                throw new UserFriendlyException(BookingBusiness.DatesCantBeEmpty);
            }

            if (DateTime.Compare(booking.Start, booking.Finish) > 0)
            {
                throw new UserFriendlyException(BookingBusiness.FirstDateShallBeLowerThanFinishDate);
            }
        }

        protected async Task CheckPlaceRelation(Booking booking)
        {
            if (booking.PlaceId == 0)
            {
                throw new UserFriendlyException(BookingBusiness.YouCanNotRegisterABookingWithOutPlace);
            }

            var place = await Place.GetAsync(booking.PlaceId);

            if (place == null)
            {
                throw new UserFriendlyException(BookingBusiness.YouCanNotRegisterABookingWithOutPlace);
            }
        }
        #endregion

        #region CRUD Overrides
        public async Task<BookingDto> GetFull(EntityDto<long> input)
        {
            CheckGetPermission();

            var bookingQ = Repository
                .GetAllIncluding(
                    b => b.Phones,
                    b => b.Place,
                    b => b.Transactions,
                    b => b.Fees
                )
                .Where(b => b.Id == input.Id);

            var booking = bookingQ.First();
            var bookingDto = MapToEntityDto(booking);

            // Exapand Childres. Fro some reason GetAllIncluding is not work properly
            //bookingDto.Place = booking.PlaceId > 0
            //    ? ObjectMapper.Map<PlaceDto>(booking.Place)
            //    : null;

            //bookingDto.Transactions = booking.Transactions.Count() > 0
            //    ? ObjectMapper.Map<ICollection<TransactionDto>>(booking.Transactions)
            //    : null;

            //bookingDto.Phones = booking.Phones.Count() > 0
            //    ? ObjectMapper.Map<ICollection<BookingPhoneDto>>(booking.Phones)
            //    : null;

            return ObjectMapper.Map<BookingDto>(bookingQ.First());
        }

        public async Task<PagedResultDto<BookingDto>> GetAllFromDates(SearchAvailableBookingDatesDto input)
        {
            CheckGetAllPermission();

            var query = CreateQueryAvailableDates(input);
            var mappedInput = ObjectMapper.Map<PagedAndSortedBookingRequestDto>(input);
            query = ApplySorting(query, mappedInput);
            query = ApplyPaging(query, mappedInput);

            return new PagedResultDto<BookingDto> (
                query.Count(),
                query.Cast<BookingDto>().ToList());
        }

        public async Task<bool> IsAvalidable(SearchAvailableBookingDatesDto input)
        {
            CheckGetPermission();

            return CreateQueryAvailableDates(input).Count() == 0;
        }

        public async Task<BookingDto> Cancel(EntityDto<long> input)
        {
            CheckUpdatePermission();

            var booking = await Repository.GetAsync(input.Id);
            booking.Status = BookingStatus.Canceled;

            await Repository.UpdateAsync(booking);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(booking);
        }

        public async Task<BookingDto> Confirm(EntityDto<long> input)
        {
            CheckUpdatePermission();

            var booking = await Repository.GetAsync(input.Id);
            booking.Status = BookingStatus.Confirmed;

            await Repository.UpdateAsync(booking);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(booking);
        }

        public async Task<BookingDto> UpdateDates(BookingDatesDto input)
        {
            CheckUpdatePermission();

            var booking = await Repository.GetAsync(input.Id);

            booking.Start = input.Start.Value;
            booking.Finish = input.Finish.Value;

            await CheckBookingDates(booking);
            await CheckBookingAvailableDates(booking);
            await CheckPlaceRelation(booking);

            Repository.Update(booking);

            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(booking);
        }

        public override async Task<BookingDto> Create(CreateBookingDto input)
        {
            CheckCreatePermission();

            var booking = MapToEntity(input);

            await CheckBookingFees(booking);
            await CheckBookingDates(booking);
            await CheckBookingAvailableDates(booking);
            await CheckPlaceRelation(booking);
            await CheckFinancialValues(booking);

            await Repository.InsertAsync(booking);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(booking);
        }

        protected async Task CheckBookingFees(Booking booking)
        {
            foreach (var input in booking.Fees)
            {
                var fee = UserFee
                    .GetAll()
                    .Where(f => f.UserId == input.UserId && f.PlaceId == null || f.UserId == input.UserId && f.PlaceId == booking.PlaceId)
                    .OrderBy(f => f.PlaceId)
                    .OrderByDescending(f => f.PlaceId.HasValue)
                    .First();

                input.FeeId = fee.Id;
                input.Ammount = (input.Ammount > 0)
                    ? input.Ammount
                    : booking.Ammount * fee.Percent;
            }
        }

        public override async Task<BookingDto> Update(UpdateBookingDto input)
        {
            CheckUpdatePermission();

            var booking = await Repository.GetAsync(input.Id);

            booking = ObjectMapper.Map<UpdateBookingDto, Booking>(input, booking);

            await CheckFinancialValues(booking);

            await Repository.UpdateAsync(booking);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(booking);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            CheckDeletePermission();

            var booking = await Repository.GetAsync(input.Id);

            // Check Has Confirmed Transactions
            await CheckHasConfirmedTransactionsAsync(booking);

            // Clear All Fees
            await UserFeeBooking.DeleteAsync(ufb => ufb.BookingId == booking.Id && ufb.Fee.PlaceId == booking.PlaceId);
            // Clear Transactions
            await Transaction.DeleteAsync(ufb => ufb.BookingId == booking.Id);
            // Remove Booking
            await Repository.DeleteAsync(booking);

            await CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion

        #region Filters
        protected override IQueryable<Booking> CreateFilteredQuery(PagedAndSortedBookingRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            query = AuditedQueryParser.Parse(query.Cast<IFullAudited>(), input).Cast<Booking>();
            query = ApplyDateFilter(query, input);
            query = ApplyNameFilter(query, input);
            query = ApplyPlaceFilter(query, input);
            query = ApplyPhoneFilter(query, input);
            query = ApplySourceFilter(query, input);
            query = ApplyTransactionFilter(query, input);
            query = ApplyFeeFilter(query, input);
            query = ApplyStatusFilter(query, input);

            return query;
        }

        protected IQueryable<Booking> ApplyDateFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.Start.HasValue)
            {
                input.Start = new DateTime(
                    input.Start.Value.Year,
                    input.Start.Value.Month,
                    input.Start.Value.Day,
                    0,0,0
                );

                query = query.Where(b => b.Start >= input.Start || b.Start < input.Start && b.Finish >= input.Start);
            }

            if (input.Finish.HasValue)
            {
                input.Finish = new DateTime(
                    input.Finish.Value.Year,
                    input.Finish.Value.Month,
                    input.Finish.Value.Day,
                    23, 59, 59
                );

                query = query.Where(b => b.Finish <= input.Finish || b.Finish >= input.Finish && b.Start <= input.Finish);
            }

            return query;
        }

        protected IQueryable<Booking> ApplyNameFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Name))
            {
                query = query.Where(b => b.Name.ToLower().Contains(input.Name.ToLower()));
            }

            return query;
        }

        protected IQueryable<Booking> ApplyEmailFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Email))
            {
                query = query.Where(b => b.Email.ToLower().Contains(input.Email.ToLower()));
            }

            return query;
        }

        protected IQueryable<Booking> ApplyPlaceFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.PlaceId.HasValue)
            {
                query = query.Where(b => b.PlaceId == input.PlaceId);
            }

            if (!String.IsNullOrEmpty(input.PlaceSearch))
            {
                query = query.Where(b => b.Place.Name.ToLower().Contains(input.PlaceSearch.ToLower()));
            }

            return query;
        }
        
        protected IQueryable<Booking> ApplyPhoneFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.PhoneId.HasValue)
            {
                query = query.SelectMany(b => b.Phones.Where(pb => pb.Id == input.PhoneId)).Select(pb => pb.Booking);
            }

            if (input.Phone != null)
            {
                query = query.SelectMany(b => b.Phones.Where(
                    pb => pb.Phone == input.Phone
                )).Select(pb => pb.Booking);
            }

            return query;
        }

        protected IQueryable<Booking> ApplySourceFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.SourceId.HasValue)
            {
                query = query.Where(b => b.Source.Id == input.SourceId);
            }

            if (!String.IsNullOrEmpty(input.SourceSearch))
            {
                query = query.Where(
                    b => b.Source.Name.ToLower().Contains(input.SourceSearch.ToLower()) 
                      || b.Source.Url.ToLower().Contains(input.SourceSearch.ToLower())
                );
            }

            return query;
        }
        
        protected IQueryable<Booking> ApplyTransactionFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.TransactionId.HasValue)
            {
                query = query.SelectMany(b => b.Transactions.Where(t => t.Id == input.TransactionId)).Select(t => t.Booking);
            }

            if (input.TransactionDate.HasValue)
            {
                var start = new DateTime(
                    input.TransactionDate.Value.Year,
                    input.TransactionDate.Value.Month,
                    input.TransactionDate.Value.Day,
                    0, 0, 0);
                var finish = start.AddDays(1);

                query = query.SelectMany(b => b.Transactions.Where(t => t.MatureAt >= start && t.MatureAt < finish)).Select(t => t.Booking);
            }

            if (input.TransactionValue.HasValue)
            {
                query = query.SelectMany(b => b.Transactions.Where(t => t.Ammount.ToString().Contains(input.TransactionValue.ToString()))).Select(t => t.Booking);
            }

            return query;
        }
        
        protected IQueryable<Booking> ApplyFeeFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.UserId.HasValue)
            {
                query = query.SelectMany(b => b.Fees.Where(bf => bf.UserId == input.UserId)).Select(bf => bf.Booking);
            }

            if (!String.IsNullOrEmpty(input.UserSearch))
            {
                query = query.SelectMany(b => b.Fees.Where(
                    bf => bf.User.Name.ToLower().Contains(input.UserSearch.ToLower())
                          ||
                          bf.User.UserName.ToLower().Contains(input.UserSearch.ToLower())
                          ||
                          bf.User.EmailAddress.ToLower().Contains(input.UserSearch.ToLower()))).Select(bf => bf.Booking);
            }

            return query;
        }

        protected IQueryable<Booking> ApplyStatusFilter(IQueryable<Booking> query, PagedAndSortedBookingRequestDto input)
        {
            if (input.Status.HasValue)
            {
                query = query.Where(b => b.Status == input.Status);
            }

            return query;
        }
        #endregion
    }
}
