﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using Booking.Authorization.Users;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using Booking.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Booking
{
    public static class UserFeeBookingBusiness
    {
        public const string YouShallInformAnFeeOrUserReferente = "userFeeBooking.youShallInformAnFeeOrUserReference";
        public const string YouCanRegisterOnlyOneFeePerUser = "userFeeBooking.youCanRegisterOnlyOneFeePerUser";
        public const string YouCanNotRemoveFeeWithConfirmedTransactions = "userFeeBooking.youCanNotRemoveFeeWithConfirmedTransactions";
    }

    [AbpAuthorize]
    public class UserFeeBookingCrudService : AsyncCrudAppService<UserFeeBooking, UserFeeBookingDto, long, PagedAndSortedUserFeeBookingRequestDto, CreateUserFeeBookingDto, UpdateUserFeeBookingDto, GetUserFeeBookingDto, GetUserFeeBookingDto>, IUserFeeBookingCrudService
    {
        protected readonly IRepository<Booking, long> BookingRepo;
        protected readonly IRepository<User, long> UserRepo;
        protected readonly IRepository<UserFee, long> FeeRepo;
        protected readonly IRepository<Transaction, long> TransactionRepo;

        public UserFeeBookingCrudService(
            IRepository<UserFeeBooking, long> repository,
            IRepository<Booking, long> bookingRepo,
            IRepository<User, long> userRepo,
            IRepository<UserFee, long> userFeeRepo,
            IRepository<Transaction, long> transactionRepo
        ) : base(repository)
        {
            BookingRepo = bookingRepo;
            UserRepo = userRepo;
            FeeRepo = userFeeRepo;
            TransactionRepo = transactionRepo;
        }

        #region Custom Queries
        public async Task<UserFeeBookingDto> GetFull(GetUserFeeBookingDto input)
        {
            var feeQ = Repository
                .GetAllIncluding(
                    ufb => ufb.User,
                    ufb => ufb.Booking,
                    ufb => ufb.Fee
                )
                .Where(ufb => ufb.BookingId == input.BookingId && ufb.UserId == input.UserId);

            if (input.Id > 0)
            {
                feeQ = feeQ.Where(ufb => ufb.Id == input.Id);
            }

            return ObjectMapper.Map<UserFeeBookingDto>(feeQ.First());
        }

        public override async Task<UserFeeBookingDto> Get(GetUserFeeBookingDto input)
        {
            var feeQ = Repository
                .GetAll()
                .Where(ufb => ufb.BookingId == input.BookingId && ufb.UserId == input.UserId);

            if (input.Id > 0)
            {
                feeQ = feeQ.Where(ufb => ufb.Id == input.Id);
            }

            return ObjectMapper.Map<UserFeeBookingDto>(feeQ.First());
        }
        #endregion

        #region Filters

        protected IQueryable<UserFeeBooking> ApplyUserFilter(IQueryable<UserFeeBooking> query, PagedAndSortedUserFeeBookingRequestDto input)
        {
            if (input.UserId.HasValue)
            {
                query = query.Where(ufb => ufb.UserId == input.UserId);
            }

            return query;
        }

        protected IQueryable<UserFeeBooking> ApplyFeeFilter(IQueryable<UserFeeBooking> query, PagedAndSortedUserFeeBookingRequestDto input)
        {
            if (input.FeeId.HasValue)
            {
                query = query.Where(ufb => ufb.FeeId == input.FeeId);
            }

            return query;
        }

        protected IQueryable<UserFeeBooking> ApplyBookingFilter(IQueryable<UserFeeBooking> query, PagedAndSortedUserFeeBookingRequestDto input)
        {
            if (input.BookingId.HasValue)
            {
                query = query.Where(ufb => ufb.BookingId == input.BookingId);
            }

            return query;
        }

        protected override IQueryable<UserFeeBooking> CreateFilteredQuery(PagedAndSortedUserFeeBookingRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            query = ApplyBookingFilter(query, input);
            query = ApplyFeeFilter(query, input);
            query = ApplyUserFilter(query, input);

            return query;
        }
        #endregion

        #region Business
        protected async Task CheckUniqueFee(UserFeeBooking ufb)
        {
            var ufbCount = Repository
                .GetAll()
                .Where(r => r.BookingId == ufb.BookingId && r.UserId == ufb.UserId)
                .Count();

            if (ufbCount > 0)
            {
                throw new UserFriendlyException(UserFeeBookingBusiness.YouCanRegisterOnlyOneFeePerUser);
            }
        }

        protected async Task CheckUserFeeBookingKeys(UserFeeBooking ufb)
        {
            if (ufb.FeeId > 0)
            {
                ufb.Fee = await FeeRepo.GetAsync(ufb.FeeId);
            }
            else if (ufb.UserId > 0)
            {
                var booking = await BookingRepo.GetAsync(ufb.BookingId);

                ufb.Fee = FeeRepo
                    .GetAllIncluding(f => f.User)
                    .Where(f => f.UserId == ufb.UserId && f.PlaceId == booking.Place.Id || f.UserId == ufb.UserId && f.PlaceId == null)
                    .OrderBy(f => f.PlaceId)
                    .OrderByDescending(f => f.PlaceId.HasValue)
                    .First();
                ufb.FeeId = ufb.Fee.Id;
            }
            else
            {
                throw new UserFriendlyException(UserFeeBookingBusiness.YouShallInformAnFeeOrUserReferente);
            }
        }

        protected async Task CheckTransactions(UserFeeBooking ufb)
        {
            var tCount = TransactionRepo
                .GetAll()
                .Where(t => t.FeeId == ufb.FeeId && t.ConfirmationAt != null)
                .Count();

            if (tCount > 0)
            {
                throw new UserFriendlyException(UserFeeBookingBusiness.YouCanNotRemoveFeeWithConfirmedTransactions);
            }
        }
        #endregion

        #region CRUD Overrides

        public override async Task<UserFeeBookingDto> Create(CreateUserFeeBookingDto input)
        {
            CheckCreatePermission();

            var ufb = MapToEntity(input);

            await CheckUserFeeBookingKeys(ufb);
            await CheckUniqueFee(ufb);

            // Calc Ammount
            if (ufb.Ammount == 0)
            {
                ufb.Ammount = ufb.Booking.Ammount * ufb.Fee.Percent;
            }

            await Repository.InsertAndGetIdAsync(ufb);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(ufb);
        }

        public override async Task<UserFeeBookingDto> Update(UpdateUserFeeBookingDto input)
        {
            CheckUpdatePermission();

            var ufb = Repository
                .GetAll()
                .Where(r => r.BookingId == input.BookingId && r.FeeId == input.FeeId && r.UserId == input.UserId)
                .First();

            ufb.Ammount = (input.Ammount.HasValue)
                ? input.Ammount.Value
                : ufb.Booking.Ammount * ufb.Fee.Percent;

            await Repository.UpdateAsync(ufb);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(ufb);
        }

        public override async Task Delete(GetUserFeeBookingDto input)
        {
            CheckDeletePermission();

            var ufb = Repository
                .GetAll()
                .Where(r => r.BookingId == input.BookingId && r.UserId == input.UserId)
                .First();

            await CheckTransactions(ufb);

            await Repository.DeleteAsync(ufb);
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion

    }
}
