﻿using Abp.Application.Services;
using Booking.Booking.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Booking.Contracts
{
    public interface IUserFeeBookingCrudService : IAsyncCrudAppService<UserFeeBookingDto, long, PagedAndSortedUserFeeBookingRequestDto, CreateUserFeeBookingDto, UpdateUserFeeBookingDto, GetUserFeeBookingDto, GetUserFeeBookingDto>
    {
        Task<UserFeeBookingDto> GetFull(GetUserFeeBookingDto input);
    }
}
