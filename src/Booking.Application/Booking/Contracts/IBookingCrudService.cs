﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Booking.Booking.Dtos;
using Booking.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Booking.Contracts
{
    public interface IBookingCrudService : IAsyncCrudAppService<BookingDto, long, PagedAndSortedBookingRequestDto, CreateBookingDto, UpdateBookingDto, EntityDto<long>, EntityDto<long>>
    {
        Task<BookingDto> GetFull(EntityDto<long> input);
        Task<PagedResultDto<BookingDto>> GetAllFromDates(SearchAvailableBookingDatesDto input);
        Task<bool> IsAvalidable(SearchAvailableBookingDatesDto input);
        Task<BookingDto> Cancel(EntityDto<long> input);
        Task<BookingDto> Confirm(EntityDto<long> input);
        Task<BookingDto> UpdateDates(BookingDatesDto input);
    }
}
