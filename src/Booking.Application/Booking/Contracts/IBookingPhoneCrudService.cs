﻿using Abp.Application.Services;
using Booking.Booking.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Booking.Contracts
{
    public interface IBookingPhoneCrudService : IAsyncCrudAppService<BookingPhoneDto, long, PagedAndSortedBookingPhoneRequestDto, CreateBookingPhoneDto, UpdateBookingPhoneDto, GetBookingPhoneDto, GetBookingPhoneDto>
    {
        
    }
}
