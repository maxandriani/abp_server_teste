﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using System;
using System.Linq;
using Abp.UI;
using System.Net;
using System.Threading.Tasks;
using Abp.Authorization;

namespace Booking.Booking
{
    public static class BookingPhoneBusiness
    {
        public const string YouCanteChangeBookingReference = "bookingPhone.youCantChangeBookingReference";
    }

    [AbpAuthorize]
    public class BookingPhoneCrudService : AsyncCrudAppService<BookingPhone, BookingPhoneDto, long, PagedAndSortedBookingPhoneRequestDto, CreateBookingPhoneDto, UpdateBookingPhoneDto, GetBookingPhoneDto, GetBookingPhoneDto>, IBookingPhoneCrudService
    {
        protected readonly IRepository<Booking, long> BookingRepo;
        
        public BookingPhoneCrudService(
            IRepository<BookingPhone, long> repository,
            IRepository<Booking, long> bookingRepo
        ) : base(repository)
        {
            BookingRepo = bookingRepo;
        }

        #region Filters
        protected override IQueryable<BookingPhone> CreateFilteredQuery(PagedAndSortedBookingPhoneRequestDto input)
        {
            var query = Repository.GetAll();

            query = ApplyBookingFilter(query, input);
            query = ApplyDddFilter(query, input);
            query = ApplyDdiFilter(query, input);
            query = ApplyNumberFilter(query, input);
            query = ApplySearchFilter(query, input);

            return query;
        }

        protected IQueryable<BookingPhone> ApplySearchFilter(IQueryable<BookingPhone> query, PagedAndSortedBookingPhoneRequestDto input)
        {
            if (!String.IsNullOrEmpty(input.Search))
            {
                query = query.Where(
                    bp =>
                    (
                        bp.Phone.Ddi.ToString() + " " +
                        bp.Phone.Ddd.ToString() + " " +
                        bp.Phone.Number.ToString()
                    ).Contains(input.Search)
                );
            }

            return query;
        }

        protected IQueryable<BookingPhone> ApplyDdiFilter(IQueryable<BookingPhone> query, PagedAndSortedBookingPhoneRequestDto input)
        {
            if (input.Ddi.HasValue)
            {
                query = query.Where(bp => bp.Phone.Ddi == input.Ddi);
            }

            return query;
        }
        
        protected IQueryable<BookingPhone> ApplyDddFilter(IQueryable<BookingPhone> query, PagedAndSortedBookingPhoneRequestDto input)
        {
            if (input.Ddd.HasValue)
            {
                query = query.Where(bp => bp.Phone.Ddd == input.Ddd);
            }

            return query;
        }
        
        protected IQueryable<BookingPhone> ApplyNumberFilter(IQueryable<BookingPhone> query, PagedAndSortedBookingPhoneRequestDto input)
        {
            if (input.Number.HasValue)
            {
                query = query.Where(bp => bp.Phone.Number == input.Number);
            }

            return query;
        }
        
        protected IQueryable<BookingPhone> ApplyBookingFilter(IQueryable<BookingPhone> query, PagedAndSortedBookingPhoneRequestDto input)
        {
            if (input.BookingId.HasValue)
            {
                query = query.Where(bp => bp.BookingId == input.BookingId);
            }

            return query;
        }
        #endregion

        #region CRUD Override
        public override async Task<BookingPhoneDto> Create(CreateBookingPhoneDto input)
        {
            CheckCreatePermission();

            var phone = MapToEntity(input);
            var booking = BookingRepo.Get(input.BookingId);
            
            booking.Phones.Add(phone);

            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(phone);
        }

        public override Task<BookingPhoneDto> Update(UpdateBookingPhoneDto input)
        {
            CheckUpdatePermission();

            var booking = BookingRepo.Get(input.BookingId);
            var phone = ObjectMapper.Map<BookingPhone>(input);

            CheckBookingReference(booking, phone);

            return base.Update(input);
        }

        public override async Task Delete(GetBookingPhoneDto input)
        {
            CheckDeletePermission();

            var phone = Repository.GetAll()
                .Where(bp => bp.Id == input.Id && bp.BookingId == input.BookingId)
                .First();

            await Repository.DeleteAsync(phone);
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion

        #region Business
        protected void CheckBookingReference(Booking booking, BookingPhone phone)
        {
            var check = booking.Phones.Where(bp => bp.Id == phone.Id).First();

            if (check == null)
            {
                throw new UserFriendlyException((int)HttpStatusCode.BadRequest, BookingPhoneBusiness.YouCanteChangeBookingReference);
            }
        }
        #endregion
    }
}
