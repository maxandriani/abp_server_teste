﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Booking.Authorization;

namespace Booking
{
    [DependsOn(
        typeof(BookingCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class BookingApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<BookingAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(BookingApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
