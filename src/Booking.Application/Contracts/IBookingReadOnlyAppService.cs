﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Contracts
{
    public interface IBookingReadOnlyAppService<TEntityDto, TPrimaryKey, in TGetAllInput, in TGetInput> : IApplicationService
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TGetInput : IEntityDto<TPrimaryKey>
    {
        Task<TEntityDto> Get(TGetInput input);
        Task<PagedResultDto<TEntityDto>> GetAll(TGetAllInput input);
    }
}
