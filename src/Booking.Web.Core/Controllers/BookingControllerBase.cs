using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Booking.Controllers
{
    public abstract class BookingControllerBase: AbpController
    {
        protected BookingControllerBase()
        {
            LocalizationSourceName = BookingConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
