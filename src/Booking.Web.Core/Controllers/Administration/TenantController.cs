﻿using Abp.Application.Services.Dto;
using Booking.MultiTenancy;
using Booking.MultiTenancy.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Administration
{
    [Route("v1/tenants")]
    public class TenantController : BookingControllerBase
    {

        protected readonly ITenantAppService Tenants;

        public TenantController(
            ITenantAppService tenants
        ) : base()
        {
            Tenants = tenants;
        }

        [HttpGet]
        public Task<PagedResultDto<TenantDto>> GetTenants([FromQuery] PagedTenantResultRequestDto input)
        {
            return Tenants.GetAll(input);
        }

        [HttpPost]
        public Task<TenantDto> CreateTenant([FromBody] CreateTenantDto input)
        {
            return Tenants.Create(input);
        }

        [HttpGet]
        [Route("{{tenantId:int}}")]
        public Task<TenantDto> GetTenant(int tenantId)
        {
            return Tenants.Get(new EntityDto<int> { Id = tenantId });
        }

        [HttpPut]
        [Route("{{tenantId:int}}")]
        public Task<TenantDto> UpdateTenant(int tenantId, [FromBody] TenantDto input)
        {
            input.Id = tenantId;
            return Tenants.Update(input);
        }

        [HttpDelete]
        [Route("{{tenantId:int}}")]
        public Task DeleteTenant(int tenantId)
        {
            return Tenants.Delete(new EntityDto<int> { Id = tenantId });
        }

    }
}
