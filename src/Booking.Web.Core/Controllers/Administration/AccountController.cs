﻿using Booking.Authorization.Accounts;
using Booking.Authorization.Accounts.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Administration
{
    [Route("v1/accounts")]
    public class AccountController : BookingControllerBase
    {

        protected readonly IAccountAppService Accounts;

        public AccountController(
            IAccountAppService accounts
        ): base()
        {
            Accounts = accounts;
        }

        [HttpGet]
        [Route("tenants/{{tenancyName:string}}")]
        public Task<IsTenantAvailableOutput> GetIsTenantAvailable(string tenancyName)
        {
            return Accounts.IsTenantAvailable(new IsTenantAvailableInput { TenancyName = tenancyName });
        }

        [HttpPost]
        public Task<RegisterOutput> CreateAccount([FromBody] RegisterInput input)
        {
            return Accounts.Register(input);
        }
    }
}
