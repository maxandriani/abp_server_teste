﻿using Abp.Application.Services.Dto;
using Booking.Roles.Dto;
using Booking.Users;
using Booking.Users.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Administration
{
    [Route("v1/users")]
    public class UserController : BookingAppServiceBase
    {

        protected readonly IUserAppService UserService;

        public UserController(
            IUserAppService users
        ) : base()
        {
            UserService = users;
        }

        [HttpGet]
        public Task<PagedResultDto<UserDto>> GetUsers(PagedUserResultRequestDto input)
        {
            return UserService.GetAll(input);
        }

        [HttpPost]
        public Task<UserDto> CreateUser([FromBody] CreateUserDto input)
        {
            return UserService.Create(input);
        }

        [HttpGet]
        [Route("{userId:long}")]
        public Task<UserDto> GetUser(long userId)
        {
            return UserService.Get(new EntityDto<long> { Id = userId });
        }

        [HttpPut]
        [Route("{userId:long}")]
        public Task<UserDto> UpdateUser(long userId, [FromBody] UserDto input)
        {
            input.Id = userId;
            return UserService.Update(input);
        }

        [HttpDelete]
        [Route("{userId:long}")]
        public Task DeleteUser(long userId)
        {
            return UserService.Delete(new EntityDto<long> { Id = userId });
        }

        [HttpGet]
        [Route("{userId:long}/roles")]
        public Task<ListResultDto<RoleDto>> GetUserRoles(long userId)
        {
            return UserService.GetRoles();
        }

        [HttpPost]
        [Route("{userId:long}/change-language")]
        public Task ChangeLanguage(long userId, [FromBody] ChangeUserLanguageDto input)
        {
            return UserService.ChangeLanguage(input);
        }

        [HttpPost]
        [Route("{userId:long}/change-password")]
        public Task ChangePassword(long userId, [FromBody] ChangePasswordDto input)
        {
            return UserService.ChangePassword(input);
        }

        [HttpPost]
        [Route("{userId:long}/reset-password")]
        public Task ResetPassword(long userId, [FromBody] ResetPasswordDto input)
        {
            input.UserId = userId;
            return UserService.ResetPassword(input);
        }

    }
}
