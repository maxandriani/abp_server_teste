﻿using Booking.Configuration;
using Booking.Configuration.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Administration
{
    [Route("v1/configurations")]
    public class ConfigurationController : BookingControllerBase
    {

        protected IConfigurationAppService Configurations;

        public ConfigurationController(
            IConfigurationAppService configurations
        ) : base () {
            Configurations = configurations;
        }

        [HttpPost]
        public Task ChangeUiTheme([FromBody] ChangeUiThemeInput input)
        {
            return Configurations.ChangeUiTheme(input);
        }
    }
}
