﻿using Abp.Application.Services.Dto;
using Booking.Roles;
using Booking.Roles.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Administration
{
    [Route("v1/roles")]
    public class RoleController : BookingControllerBase
    {

        protected readonly IRoleAppService Roles;

        public RoleController(
            IRoleAppService roles
        ) : base()
        {
            Roles = roles;
        }

        [HttpGet]
        public Task<PagedResultDto<RoleDto>> GetRoles([FromQuery] PagedRoleResultRequestDto input)
        {
            return Roles.GetAll(input);
        }

        [HttpPost]
        public Task<RoleDto> CreateRole([FromBody] CreateRoleDto input)
        {
            return Roles.Create(input);
        }

        [HttpGet]
        [Route("{roleId:int}")]
        public Task<RoleDto> GetRole(int roleId)
        {
            return Roles.Get(new EntityDto<int> { Id = roleId });
        }

        [HttpPut]
        [Route("{roleId:int}")]
        public Task<RoleDto> UpdateRole(int roleId, [FromBody] RoleDto input)
        {
            input.Id = roleId;
            return Roles.Update(input);
        }

        [HttpDelete]
        [Route("{roleId:int}")]
        public Task DeleteRole(int roleId)
        {
            return Roles.Delete(new EntityDto<int> { Id = roleId });
        }

        [HttpGet]
        [Route("{roleId:int}/full")]
        public Task<GetRoleForEditOutput> GetRoleFull(int roleId)
        {
            return Roles.GetRoleForEdit(new EntityDto { Id = roleId });
        }

        [HttpGet]
        [Route("permissions")]
        public Task<ListResultDto<PermissionDto>> GetPermissions()
        {
            return Roles.GetAllPermissions();
        }

        [HttpGet]
        [Route("permissions/{permission}")]
        public Task<ListResultDto<RoleListDto>> GetRolesByPermission(string permission)
        {
            return Roles.GetRolesAsync(new GetRolesInput { Permission = permission });
        }

    }
}
