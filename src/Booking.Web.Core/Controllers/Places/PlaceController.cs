﻿using Abp.Application.Services.Dto;
using Booking.Places.Contracts;
using Booking.Places.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Booking.Controllers
{
    [Route("v1/places")]
    public class PlaceController : BookingControllerBase
    {

        #region Dependencies
        protected readonly IPlaceCrudService PlaceService;
        protected readonly ISourceCrudService SourceService;

        public PlaceController(
            IPlaceCrudService placeService,
            ISourceCrudService sourceService
        ) : base()
        {
            PlaceService = placeService;
            SourceService = sourceService;
        }
        #endregion

        [HttpGet]
        public async Task<PagedResultDto<PlaceDto>> GetPlaces([FromQuery] PagedAndSortedPlaceRequestDto filters)
        {
            return await PlaceService.GetAll(filters);
        }

        [HttpPost]
        public async Task<object> CreatePlace([FromBody] CreatePlaceDto input)
        {
            return await PlaceService.Create(input);
        }

        [HttpDelete]
        [Route("{placeId:long}")]
        public async Task DeletePlace(long placeId)
        {
            await PlaceService.Delete(new EntityDto<long> { Id = placeId });
        }

        [HttpPut]
        [Route("{placeId:long}")]
        public async Task<PlaceDto> UpdatePlace(long placeId, [FromBody] UpdatePlaceDto input)
        {
            input.Id = placeId;
            return await PlaceService.Update(input);
        }

        [HttpGet]
        [Route("{placeId:long}")]
        public async Task<PlaceDto> GetPlace(long placeId)
        {
            return await PlaceService.GetPlaceFullAsync(new EntityDto<long> { Id = placeId });
        }
        
    }
}