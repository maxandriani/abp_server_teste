﻿using Abp.Application.Services.Dto;
using Booking.Places.Contracts;
using Booking.Places.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Places
{
    [Route("v1/places/{placeId:long}/sources")]
    public class PlaceSourceController : BookingControllerBase
    {
        protected ISourceCrudService SourceService;

        public PlaceSourceController(
            ISourceCrudService sourceService
        ) : base()
        {
            SourceService = sourceService;
        }

        [HttpGet]
        public Task<PagedResultDto<SourceDto>> GetSources([FromQuery] PagedAndSortedSourceRequestDto input, long placeId)
        {
            input.Place = placeId;
            return SourceService.GetAll(input);
        }

        [HttpPost]
        public Task<SourceDto> CreateSource(long placeId, [FromBody] CreatePlaceSourceDto input)
        {
            var source = new SourceDto { Url = input.Url, Name = input.Name, PlaceId = placeId };

            return SourceService.Create(source);
        }

        [HttpGet]
        [Route("{sourceId:long}")]
        public Task<SourceDto> GetSource(long placeId, long sourceId)
        {
            return SourceService.Get(new GetSourceDto { Id = sourceId, PlaceId = placeId });
        }

        [HttpPut]
        [Route("{sourceId:long}")]
        public Task<SourceDto> UpdateSource(long placeId, long sourceId, [FromBody] CreatePlaceSourceDto input)
        {
            var source = new UpdateSourceDto { Url = input.Url, Name = input.Name, Id = sourceId, PlaceId = placeId };

            return SourceService.Update(source);
        }

        [HttpDelete]
        [Route("{sourceId:long}")]
        public Task DeleteSource(long placeId, long sourceId)
        {
            return SourceService.Delete(new GetSourceDto { Id = sourceId, PlaceId = placeId });
        }
    }
}
