﻿using Abp.Application.Services.Dto;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Finantial
{
    [Route("v1/wallets")]
    public class WalletController : BookingControllerBase
    {
        protected readonly IWalletCrudService WalletService;
        protected readonly IBalanceService BalanceService;

        public WalletController(
            IWalletCrudService walletService,
            IBalanceService balanceService
        ) : base() {
            WalletService = walletService;
            BalanceService = balanceService;
        }

        #region CRUD Routes
        [HttpGet]
        public Task<PagedResultDto<WalletDto>> GetWallets(PagedAndSortedWalletRequestDto input)
        {
            return WalletService.GetAll(input);
        }

        [HttpPost]
        public Task<WalletDto> CreateWallet([FromBody] CreateWalletDto input)
        {
            return WalletService.Create(input);
        }

        [HttpGet]
        [Route("{walletId:long}")]
        public Task<WalletDto> GetWallet(long walletId)
        {
            return WalletService.Get(new EntityDto<long> { Id = walletId });
        }

        [HttpPut]
        [Route("{walletId:long}")]
        public Task<WalletDto> UpdateWallet(long walletId, [FromBody] CreateWalletDto input)
        {
            var wallet = new WalletDto { Id = walletId, Default = input.Default, Description = input.Description };

            return WalletService.Update(wallet);
        }

        [HttpDelete]
        [Route("{walletId:long}")]
        public Task DeleteWallet(long walletId)
        {
            return WalletService.Delete(new EntityDto<long> { Id = walletId });
        }

        [HttpGet]
        [Route("default")]
        public Task<WalletDto> GetDefaultWallet()
        {
            return WalletService.GetDefaultWallet();
        }
        #endregion
    }
}
