﻿using Abp.Application.Services.Dto;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Finantial
{
    [Route("v1/seasons")]
    public class SeasonController : BookingControllerBase
    {
        protected readonly ISeasonCrudService SeasonService;
        protected readonly IBalanceService BalanceService;

        public SeasonController(
            ISeasonCrudService seasonService,
            IBalanceService balanceService
        ) : base ()
        {
            SeasonService = seasonService;
            BalanceService = balanceService;
        }


        [HttpGet]
        public Task<PagedResultDto<SeasonDto>> GetSeasons(PagedAndSortedSeasonRequestDto input)
        {
            return SeasonService.GetAll(input);
        }

        [HttpPost]
        public Task<SeasonDto> CreateSeason([FromBody] CreateSeasonDto season)
        {
            return SeasonService.Create(season);
        }

        [HttpGet]
        [Route("{seasonId:long}")]
        public Task<SeasonDto> GetSeason(long seasonId)
        {
            return SeasonService.Get(new EntityDto<long> { Id = seasonId });
        }

        [HttpPut]
        [Route("{seasonId:long}")]
        public Task<SeasonDto> UpdateSeason(long seasonId, [FromBody] CreateSeasonDto input)
        {
            var season = new SeasonDto {
                Id = seasonId,
                Start = input.Start,
                Finish = input.Finish,
                Description = input.Description
            };

            return SeasonService.Update(season);
        }

        [HttpDelete]
        [Route("{seasonId:long}")]
        public Task DeleteSeason(long seasonId)
        {
            return SeasonService.Delete(new EntityDto<long> { Id = seasonId });
        }
    }
}
