﻿using Abp.Application.Services.Dto;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Finantial
{
    [Route("v1/transactions")]
    public class TransactionController : BookingControllerBase
    {

        protected readonly ITransactionCrudService Transactions;

        public TransactionController(
            ITransactionCrudService transactionService
        ) : base()
        {
            Transactions = transactionService;
        }

        [HttpGet]
        public Task<PagedResultDto<TransactionDto>> GetTransactions(PagedAndSortedTransactionRequestDto input)
        {
            return Transactions.GetAll(input);
        }

        [HttpPost]
        public Task<TransactionDto> CreateTransaction([FromBody] CreateTransactionDto input)
        {
            return Transactions.Create(input);
        }

        [HttpGet]
        [Route("{transactionId:long}")]
        public Task<TransactionDto> GetFullTransaction(long transactionId)
        {
            return Transactions.Get(new EntityDto<long> { Id = transactionId });
        }

        [HttpPut]
        [Route("{transactionId:long}")]
        public Task<TransactionDto> UpdateTransaction(long transactionId, [FromBody] UpdateTransactionDto input)
        {
            input.Id = transactionId;
            return Transactions.Update(input);
        }

        [HttpDelete]
        [Route("{transactionId:long}")]
        public Task DeleteTransaction(long transactionId)
        {
            return Transactions.Delete(new EntityDto<long> { Id = transactionId });
        }

        [HttpPost]
        [Route("{transactionId:long}/confirm")]
        public Task ConfirmTransaction(long transactionId, [FromBody] ConfirmTransactionDto input)
        {
            input.Id = transactionId;
            return Transactions.Confirm(input);
        }

        [HttpPost]
        [Route("{transactionId:long}/cancel")]
        public Task CancelTransaction(long transactionId)
        {
            return Transactions.Cancel(new EntityDto<long> { Id = transactionId });
        }

        [HttpPost]
        [Route("{transactionId:long}/rollback")]
        public Task RollbackTransaction(long transactionId)
        {
            return Transactions.Rollback(new EntityDto<long> { Id = transactionId });
        }

        [HttpPost]
        [Route("generate/booking")]
        public Task<PagedResultDto<TransactionDto>> GenerateFromBooking([FromBody] GenerateTransactionFromBookingDto input)
        {
            return Transactions.GenerateFromBooking(input);
        }

        [HttpPost]
        [Route("generate/fee")]
        public Task<PagedResultDto<TransactionDto>> GenerateFromFee([FromBody] GenerateTransactionFromFeeDto input)
        {
            return Transactions.GenerateFromFee(input);
        }
    }
}
