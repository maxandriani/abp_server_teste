﻿using Abp.Application.Services.Dto;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Finantial
{
    [Route("v1/fees")]
    public class UserFeeController : BookingControllerBase
    {
        protected readonly IUserFeeCrudService UserFeeService;

        public UserFeeController(
            IUserFeeCrudService userFeeService
        ) : base()
        {
            UserFeeService = userFeeService;
        }

        [HttpGet]
        public Task<PagedResultDto<UserFeeDto>> GetUserFees(PagedAndSortedUserFeeRequestDto input)
        {
            return UserFeeService.GetAll(input);
        }

        [HttpPost]
        public Task<UserFeeDto> CreateUserFee([FromBody] CreateUserFeeDto input)
        {
            return UserFeeService.Create(input);
        }

        [HttpGet]
        [Route("{userFeeId:long}")]
        public Task<UserFeeDto> GetUserFee(long userFeeId)
        {
            return UserFeeService.GetFull(new GetUserFeeDto { Id = userFeeId });
        }

        [HttpPut]
        [Route("{userFeeId:long}")]
        public Task<UserFeeDto> UpdateUserFee(long userFeeId, [FromBody] CreateUserFeeDto input)
        {
            var userFee = new UpdateUserFeeDto
            {
                Id = userFeeId,
                Percent = input.Percent,
                UserId = input.UserId,
                PlaceId = input.PlaceId
            };

            return UserFeeService.Update(userFee);
        }

        [HttpDelete]
        [Route("{userFeeId:long}")]
        public Task DeleteUserFee(long userFeeId)
        {
            return UserFeeService.Delete(new GetUserFeeDto { Id = userFeeId });
        }
    }
}
