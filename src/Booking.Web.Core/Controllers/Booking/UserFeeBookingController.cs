﻿using Abp.Application.Services.Dto;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Booking.Models.Booking;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Booking
{
    [Route("v1/booking/{bookingId}")]
    public class UserFeeBookingController : BookingControllerBase
    {
        protected readonly IUserFeeBookingCrudService FeeService;
        protected readonly IBalanceService BalanceService;
        protected readonly IBookingCrudService BookingService;
        protected readonly ITransactionCrudService TransactionService;

        public UserFeeBookingController(
            IUserFeeBookingCrudService feeService,
            IBalanceService balanceService,
            IBookingCrudService bookingService,
            ITransactionCrudService transactionService
        ) : base()
        {
            FeeService = feeService;
            BalanceService = balanceService;
            BookingService = bookingService;
            TransactionService = transactionService;
        }

        [HttpGet]
        [Route("fees")]
        public Task<PagedResultDto<UserFeeBookingDto>> GetFees(long bookingId, [FromQuery] PagedAndSortedUserFeeBookingRequestDto input)
        {
            input.BookingId = bookingId;
            return FeeService.GetAll(input);
        }

        [HttpGet]
        [Route("users/{userId:long}/fee")]
        public Task<UserFeeBookingDto> GetUserFee(long bookingId, long userId)
        {
            return FeeService.GetFull(new GetUserFeeBookingDto { BookingId = bookingId, UserId = userId });
        }

        [HttpPost]
        [Route("users/{userId:long}/fee")]
        public Task<UserFeeBookingDto> CreateUserFee(long bookingId, long userId, [FromBody] CreateUserFeeBookingInput input)
        {
            var userFeeBooking = new CreateUserFeeBookingDto { Ammount = input.Ammount, BookingId = bookingId, UserId = userId };
            return FeeService.Create(userFeeBooking);
        }

        [HttpDelete]
        [Route("users/{userId:long}/fee")]
        public Task DeleteUserFee(long bookingId, long userId)
        {
            return FeeService.Delete(new GetUserFeeBookingDto { BookingId = bookingId, UserId = userId });
        }

        [HttpGet]
        [Route("users/{userId:long}/fee/balance")]
        public async Task<BalanceVo> GetUserFeeBalance(long bookingId, long userId)
        {
            return new BalanceVo(0);
        }

        [HttpGet]
        [Route("users/{userId:long}/fee/transactions")]
        public async Task<PagedResultDto<TransactionDto>> GetTransactions(long bookingId, long userId, [FromQuery] PagedAndSortedTransactionRequestDto input)
        {
            var fee = await FeeService.Get(new GetUserFeeBookingDto { BookingId = bookingId, UserId = userId });
            input.FeeId = fee.Id;
            input.BookingId = bookingId;
            input.UserId = userId;

            return await TransactionService.GetAll(input);
        }

        [HttpPost]
        [Route("users/{userId:long}/fee/transactions/generate")]
        public async Task<PagedResultDto<TransactionDto>> GenerateTransactions(long bookingId, long userId, [FromBody] GenerateTransactionFromFeeDto input)
        {
            var fee = await FeeService.Get(new GetUserFeeBookingDto { UserId = userId, BookingId = bookingId });
            input.FeeId = fee.Id;

            return await TransactionService.GenerateFromFee(input);
        }
    }
}
