﻿using Abp.Application.Services.Dto;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using Booking.CommonVO;
using Booking.Models.Booking;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Booking.Controllers.Booking
{
    [Route("v1/booking/{bookingId:long}/phones")]
    public class BookingPhoneController : BookingControllerBase
    {
        protected readonly IBookingPhoneCrudService BookingPhoneService;

        public BookingPhoneController(
            IBookingPhoneCrudService bookingPhoneService
        ) : base()
        {
            BookingPhoneService = bookingPhoneService;
        }

        [HttpGet]
        public Task<PagedResultDto<BookingPhoneDto>> GetPhones(long bookingId, [FromQuery] PagedAndSortedBookingPhoneRequestDto input)
        {
            input.BookingId = bookingId;
            return BookingPhoneService.GetAll(input);
        }

        [HttpPost]
        public Task<BookingPhoneDto> CreatePhone(long bookingId, [FromBody] CreateBookingPhoneInput input)
        {
            var phone = new CreateBookingPhoneDto
            {
                BookingId = bookingId,
                Phone = new Phone(input.Ddi, input.Ddd, input.Number)
            };

            return BookingPhoneService.Create(phone);
        }

        [HttpGet]
        [Route("{phoneId:long}")]
        public Task<BookingPhoneDto> GetPhone(long bookingId, long phoneId)
        {
            return BookingPhoneService.Get(new GetBookingPhoneDto
            {
                Id = phoneId,
                BookingId = bookingId
            });
        }

        [HttpPut]
        [Route("{phoneId:long}")]
        public Task<BookingPhoneDto> UpdatePhone(long bookingId, long phoneId, [FromBody] CreateBookingPhoneInput input)
        {
            var phone = new UpdateBookingPhoneDto
            {
                Id = phoneId,
                BookingId = bookingId,
                Phone = new Phone(input.Ddi, input.Ddd, input.Number)
            };

            return BookingPhoneService.Update(phone);
        }

        [HttpDelete]
        [Route("{phoneId:long}")]
        public Task DeletePhone(long bookingId, long phoneId)
        {
            return BookingPhoneService.Delete(new GetBookingPhoneDto { Id = phoneId, BookingId = bookingId });
        }
    }
}
