﻿using Abp.Application.Services.Dto;
using Booking.Booking.Contracts;
using Booking.Booking.Dtos;
using Booking.CommonVO;
using Booking.Financial.Contracts;
using Booking.Financial.Dtos;
using Booking.Models.Booking;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Controllers.Booking
{
    [Route("v1/bookings")]
    public class BookingController : BookingControllerBase
    {
        protected readonly IBookingCrudService Booking;
        protected readonly ITransactionCrudService Transactions;

        public BookingController(
            IBookingCrudService booking,
            ITransactionCrudService transactions
        ) : base()
        {
            Booking = booking;
            Transactions = transactions;
        }

        [HttpGet]
        public Task<PagedResultDto<BookingDto>> GetBookings([FromQuery] PagedAndSortedBookingRequestDto input)
        {
            return Booking.GetAll(input);
        }

        [HttpPost]
        public Task<BookingDto> CreateBooking([FromBody] CreateBookingInput input)
        {
            var booking = ObjectMapper.Map<CreateBookingDto>(input);

            foreach (var userFee in input.Fees)
            {
                booking.Fees.Add(new CreateUserFeeBookingDto
                {
                    Ammount = userFee.Ammount,
                    UserId = userFee.UserId
                });
            }

            foreach (var phone in input.Phones)
            {
                booking.Phones.Add(new CreateBookingPhoneDto
                {
                    Phone = new Phone(phone.Ddi, phone.Ddd, phone.Number)
                });
            }

            return Booking.Create(booking);
        }

        [HttpPost]
        [Route("check-dates")]
        public Task<bool> IsAvailableDates([FromBody] CheckBookingDatesInput input)
        {
            return Booking.IsAvalidable(new SearchAvailableBookingDatesDto { Start = input.Start, Finish = input.Finish, PlaceId = input.PlaceId });
        }

        [HttpGet]
        [Route("{bookingId:long}")]
        public Task<BookingDto> GetBooking(long bookingId)
        {
            return Booking.GetFull(new EntityDto<long> { Id = bookingId });
        }

        [HttpPut]
        [Route("{bookingId:long}")]
        public Task<BookingDto> UpdateBooking(long bookingId, [FromBody] UpdateBookingDto input)
        {
            input.Id = bookingId;
            return Booking.Update(input);
        }

        [HttpDelete]
        [Route("{bookingId:long}")]
        public Task DeleteBooking(long bookingId)
        {
            return Booking.Delete(new EntityDto<long> { Id = bookingId });
        }

        [HttpPost]
        [Route("{bookingId:long}/cancel")]
        public Task<BookingDto> CancelBooking(long bookingId)
        {
            return Booking.Cancel(new EntityDto<long> { Id = bookingId });
        }

        [HttpPost]
        [Route("{bookingId:long}/confirm")]
        public Task<BookingDto> ConfirmBooking(long bookingId)
        {
            return Booking.Confirm(new EntityDto<long> { Id = bookingId });
        }

        [HttpGet]
        [Route("{bookingId:long}/transactions")]
        public Task<PagedResultDto<TransactionDto>> GetTransactions(long bookingId, [FromQuery] PagedAndSortedTransactionRequestDto input)
        {
            input.BookingId = bookingId;
            return Transactions.GetAll(input);
        }

        [HttpPost]
        [Route("{bookingId:long}/transactions/generate")]
        public Task<PagedResultDto<TransactionDto>> GenerateTransactions(long bookingId, [FromBody] GenerateTransactionFromBookingDto input)
        {
            input.BookingId = bookingId;
            return Transactions.GenerateFromBooking(input);
        }

        [HttpPut]
        [Route("{bookingId:long}/dates")]
        public Task<BookingDto> UpdateBookingDates(long bookingId, [FromBody] BookingDatesDto input)
        {
            input.Id = bookingId;
            return Booking.UpdateDates(input);
        }

    }
}
