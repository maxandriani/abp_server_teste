﻿using Abp.AutoMapper;
using Booking.Booking.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Models.Booking
{
    public class CreateBookingInput
    {
        #region Business
        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public double? Ammount { get; set; }
        public ushort PaymentSplit { get; set; } = 1;
        #endregion

        #region Relational Properties
        public long PlaceId { get; set; }

        public long? SourceId { get; set; } /// @TODO Configure onDelete=SET NULL

        public ICollection<CreateBookingFeeInput> Fees { get; set; }

        public ICollection<CreateBookingPhoneInput> Phones { get; set; }
        #endregion

        public CreateBookingInput()
        {
            Fees = new List<CreateBookingFeeInput>();
            Phones = new List<CreateBookingPhoneInput>();
        }
    }
}
