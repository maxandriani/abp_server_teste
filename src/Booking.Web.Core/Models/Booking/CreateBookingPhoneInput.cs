﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Models.Booking
{
    public class CreateBookingPhoneInput
    {
        #region Business Properties
        public ushort Ddi { get; set; }
        public ushort Ddd { get; set; }
        public int Number { get; set; }
        #endregion
    }
}
