﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Models.Booking
{
    public class CheckBookingDatesInput
    {
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public long? PlaceId { get; set; }
    }
}
