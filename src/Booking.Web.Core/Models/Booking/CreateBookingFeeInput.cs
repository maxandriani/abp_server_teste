﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Models.Booking
{
    public class CreateBookingFeeInput
    {
        #region Business properties
        public double? Ammount { get; set; }
        #endregion

        #region Relational prperties
        public long UserId { get; set; }
        #endregion
    }
}
