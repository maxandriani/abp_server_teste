﻿using Abp.Authorization;
using Booking.Authorization.Roles;
using Booking.Authorization.Users;

namespace Booking.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
