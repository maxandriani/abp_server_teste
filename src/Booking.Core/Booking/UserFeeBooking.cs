﻿using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using Booking.Authorization.Users;
using Booking.Financial;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Booking
{
    public static class UserFeeBookingErrors
    {
        #region Error Properties
        public const string UserMustBeTheSameOfFee = "finantial.userFeeBooking.userMustBeTheSamOfFee";
        #endregion
    }

    [Table("user_fee_booking", Schema = "booking")]
    public class UserFeeBooking : Entity<long>
    {
        #region Business properties
        [Required]
        public virtual double Ammount { get; set; }

        #endregion

        #region Relational prperties
        public virtual User User { get; set; }
        public virtual long UserId { get; set; }

        public virtual UserFee Fee { get; set; }
        public virtual long FeeId { get; set; }

        public virtual Booking Booking { get; set; }
        public virtual long BookingId { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

        #endregion

        #region Constructors
        public UserFeeBooking()
        {

        }

        public UserFeeBooking(Booking booking, User user): this()
        {
            Booking = booking;
            User = user;
        }

        public UserFeeBooking(Booking booking, User user, double ammount): this(booking, user)
        {
            Ammount = ammount;
        }

        public UserFeeBooking(Booking booking, UserFee fee): this(booking, fee.User)
        {
            Fee = fee;
        }

        public UserFeeBooking(Booking booking, UserFee fee, double ammount): this(booking, fee.User, ammount)
        {
            Fee = fee;
        }
        #endregion
    }
}
