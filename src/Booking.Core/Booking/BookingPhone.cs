﻿using Abp.Domain.Entities;
using Booking.CommonVO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Booking
{
    [Table("booking_phone", Schema = "booking")]
    public class BookingPhone : Entity<long>
    {
        #region Business Properties
        [Required]
        public virtual Phone Phone { get; set; }

        #endregion

        #region Relational Properties
        public virtual Booking Booking { get; set; }
        public virtual long BookingId { get; set; }
        #endregion

        #region Constructors
        public BookingPhone()
        {

        }

        public BookingPhone(Phone phone): this()
        {
            Phone = phone;
        }

        public BookingPhone(ushort? ddi, ushort? ddd, int? phone) : this(new Phone((ushort)ddi, (ushort)ddd, (int)phone))
        {

        }

        #endregion
    }
}
