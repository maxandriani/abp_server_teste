﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using Booking.CommonVO;
using Booking.Financial;
using Booking.Places;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Booking
{
    public static class BookingRules
    {
        #region Validation Properties
        public const int MaxNameLength = 128;
        public const int MaxEmailLength = 32 * 1028; // 32kb
        #endregion
    }

    public enum BookingStatus
    {
        Waiting = 1,
        Confirmed = 2,
        Canceled = 3
    }

    [Table("booking", Schema = "booking")]
    public class Booking : Entity<long>, IFullAudited
    {
        #region Audition Properties
        public virtual long? DeleterUserId { get; set; }
        public virtual DateTime? DeletionTime { get; set; }
        public virtual bool IsDeleted { get; set; }
        public virtual long? CreatorUserId { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual DateTime? LastModificationTime { get; set; }
        #endregion

        #region Business Properties
        [Required]
        public virtual DateTime Start { get; set; }

        [Required]
        public virtual DateTime Finish { get; set; }

        [Required]
        public virtual BookingStatus Status { get; set; } = BookingStatus.Waiting;

        [MaxLength(BookingRules.MaxNameLength)]
        public virtual string Name { get; set; }

        [MaxLength(BookingRules.MaxEmailLength)]
        [EmailAddress]
        public virtual string Email { get; set; }

        [Required]
        public virtual double Ammount { get; set; }

        public virtual ushort PaymentSplit { get; set; } = 1;
        #endregion

        #region Relational Properties
        public virtual Place Place { get; set; }
        [Required]
        public virtual long PlaceId { get; set; }

        public virtual Source Source { get; set; }

        public virtual ICollection<BookingPhone> Phones { get; set; }
        
        public virtual ICollection<Transaction> Transactions { get; set; }

        public virtual ICollection<UserFeeBooking> Fees { get; set; }

        #endregion

        #region Constructors
        public Booking() {
            CreationTime = Clock.Now;
        }

        public Booking(DateTime start, DateTime finish): this()
        {
            Start = start;
            Finish = finish;
        }

        public Booking(DateTime start, DateTime finish, Place place) : this(start, finish)
        {
            Place = place;
        }

        public Booking(DateTime start, DateTime finish, Place place, string name, string email): this(start, finish, place)
        {
            Name = name;
            Email = email;
        }

        #endregion

        #region Property methods
        public void addPhone(Phone phone)
        {
            var bphone = new BookingPhone(phone);
            bphone.Booking = this;
            bphone.BookingId = this.Id;
            Phones.Add(bphone);
        }

        public void removePhone(Phone phone)
        {
            foreach (BookingPhone bp in Phones)
            {
                if (bp.Phone == phone)
                {
                    Phones.Remove(bp);
                    return;
                }
            }
        }
        #endregion
    }
}
