﻿namespace Booking
{
    public class BookingConsts
    {
        public const string LocalizationSourceName = "Booking";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
