﻿using Abp.Domain.Values;
using Abp.Runtime.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Booking.CommonVO
{
    public static class PhoneRules {
        public const int MinPhoneValue = 10000;
        public const int MaxPhoneValue = 999999999;
        public const int MaxDdiValue = 999;
        public const int MaxDddValue = 999;
    }

    public static class PhoneErrors {
        public const string LowerNumberValidationError = "global.phone.toolowerNumber";
        public const string InvalidStringNumberError = "global.phone.invalidString";
    }
    public class Phone : ValueObject
    {

        [Range(0, PhoneRules.MaxDddValue)]
        public ushort? Ddi { get; protected set; }

        [Range(0, PhoneRules.MaxDddValue)]
        [Required]
        public ushort? Ddd { get; protected set; }

        [Range(0, PhoneRules.MaxPhoneValue)]
        [Required]
        public int? Number { get; protected set; }

        public Phone()
        {

        }

        public Phone(ushort? ddi, ushort? ddd, int? number): this()
        {
            Ddi = ddi;
            Ddd = ddd;
            Number = number;
        }

        public Phone(string phone): this()
        {
            var cleanReg = new Regex(@"([^\d\(\)])+");
            var phoneStr = cleanReg.Replace(phone, "");

            if (phoneStr.Length <= 9)
            {
                throw new AbpValidationException(PhoneErrors.LowerNumberValidationError);
            }

            var extractReg = new Regex(@"(?:\((\d+)\))?\s?(?:\((\d+)\))?\s?(\d+)$");
            var matches = extractReg.Matches(phoneStr);

            if (matches.Count < 2)
            {
                throw new AbpValidationException(PhoneErrors.InvalidStringNumberError);
            }

            if (matches[1].Equals(null))
            {
                // ddd + phone
                Ddd = ushort.Parse(matches[0].Value);
                Number = int.Parse(matches[2].Value);
            }
            else
            {
                // ddi + ddd + phone
                Ddi = ushort.Parse(matches[0].Value);
                Ddd = ushort.Parse(matches[1].Value);
                Number = ushort.Parse(matches[2].Value);
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Ddi;
            yield return Ddd;
            yield return Number;
        }

        public Phone SetDdi(ushort? ddi)
        {
            return new Phone(ddi, Ddd, Number);
        }

        public Phone SetDdd(ushort? ddd)
        {
            return new Phone(Ddi, ddd, Number);
        }

        public Phone SetNumber(int? phone)
        {
            return new Phone(Ddi, Ddd, phone);
        }
    }
}
