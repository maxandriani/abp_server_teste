﻿using Abp.Domain.Values;
using Abp.Runtime.Validation;
using System.Collections.Generic;

namespace Booking.CommonVO
{
    public static class CpfCnpjRules
    {
        public const long MaxCpfNumber = CpfRules.MaxCpfNumber;
        public const long MaxCnpjNumber = CnpjRules.MaxCnpjNumber;
    }

    public static class CpfCnpjErrors
    {
        public const string InvalidCpfCnpjNumber = "global.cpfCnpj.invalidNumber";
    }
    public class CpfCnpj : ValueObject
    {


        public long? Value { get; protected set; }

        public bool IsCpf
        {
            get
            {
                return IsCpfValue((long)Value);
            }
        }

        public bool IsCnpj
        {
            get
            {
                return IsCnpjValue((long)Value);
            }
        }

        public string DisplayValue
        {
            get
            {
                return (IsCpf)
                    ? Cpf.CpfMask((long)Value)
                    : Cnpj.CnpjMask((long)Value);
            }
        }

        CpfCnpj()
        {

        }

        CpfCnpj(long value): this()
        {
            if (value > CpfCnpjRules.MaxCnpjNumber || !IsValid(value))
            {
                throw new AbpValidationException(CpfCnpjErrors.InvalidCpfCnpjNumber);
            }

            Value = value;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }

        public static bool IsCpfValue(long cpf)
        {
            return (cpf <= CpfCnpjRules.MaxCpfNumber);
        }

        public static bool IsCnpjValue(long cnpj)
        {
            return (cnpj > CpfCnpjRules.MaxCpfNumber);
        }

        public static bool IsValid(long value)
        {
            return (IsCnpjValue(value))
                    ? Cnpj.IsValid(value)
                    : Cpf.IsValid(value);
        }

        public CpfCnpj SetValue(long value)
        {
            return new CpfCnpj(value);
        }

    }
}
