﻿using Abp.Domain.Values;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;

namespace Booking.CommonVO
{
    public static class CpfRules {
        public const long MaxCpfNumber = 99999999999;
    }

    public static class CpfErrors {
        public const string InvalidCpfValue = "global.cpf.invalidNumber";
    }

    public class Cpf : ValueObject
    {

        public long? Value { get; protected set; }

        public string DisplayValue
        {
            get
            {
                return CpfMask(Value);
            }
        }

        public static string CpfMask(long? cpf)
        {
            return (cpf != null)
                ? string.Format(@"{0:000\.000\.000\-00}", cpf)
                : "";
        }

        public long Raiz
        {
            get
            {
                var root = (double) ((Value != null) ? Value : 0L);
                return (long) Math.Floor(root / 100.0);
            }
        }

        public static bool IsValid(long cpf)
        {
            return true; /// @TODO Fazer validação
        }

        public static bool IsValid(Cpf cpf)
        {
            return IsValid((long)cpf.Value);
        }

        public Cpf() { }

        public Cpf(long cpf): this()
        {
            if (cpf > CpfRules.MaxCpfNumber || !IsValid(cpf))
            {
                throw new AbpValidationException(CpfErrors.InvalidCpfValue);
            }
            this.Value = cpf;
        }

        public Cpf(int cpf) : this((long)cpf)
        {

        }

        public Cpf(string cpf) : this(long.Parse(cpf))
        {

        }

        public Cpf SetValue(long cpf)
        {
            return new Cpf(cpf);
        }

        public Cpf SetValue(int cpf)
        {
            return SetValue((long)cpf);
        }

        public Cpf SetValue(string cpf)
        {
            return SetValue(long.Parse(cpf));
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}
