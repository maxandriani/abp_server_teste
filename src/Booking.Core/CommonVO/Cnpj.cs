﻿using Abp.Domain.Values;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;

namespace Booking.CommonVO
{
    public static class CnpjRules
    {
        public const long MaxCnpjNumber = 99999999999999;
    }

    public static class CnpjErrors
    {
        public const string InvalidCnpjValue = "global.cnpj.invalidNumber";
    }

    public class Cnpj : ValueObject
    {
        public long? Value { get; protected set; }

        public string DisplayValue
        {
            get
            {
                return CnpjMask(Value);
            }
        }

        public static string CnpjMask(long? cnpj)
        {
            return (cnpj != null)
                ? string.Format(@"{0:00\.000\.000\/0000\-00}", cnpj)
                : "";
        }

        public long Raiz
        {
            get
            {
                var raiz = (double)((Value != null) ? Value : 0L);
                return (long)Math.Floor(raiz / 1000000.0);
            }
        }

        public long Filial
        {
            get
            {
                var cnpj = (long) ((Value != null) ? Value : 0L);
                return (long)Math.Floor((cnpj - (Raiz * 1000000)) / 100.0);
            }
        }

        public static bool IsValid(long cnpj)
        {
            return true; /// @TODO Fazer validação
        }

        public static bool IsValid(Cnpj cnpj)
        {
            return IsValid((long) cnpj.Value);
        }

        public Cnpj() { }

        public Cnpj(long cnpj): this()
        {
            if (cnpj > CnpjRules.MaxCnpjNumber || !IsValid(cnpj))
            {
                throw new AbpValidationException(CnpjErrors.InvalidCnpjValue);
            }
            this.Value = cnpj;
        }

        public Cnpj(int cnpj) : this((long)cnpj)
        {

        }

        public Cnpj(string cnpj) : this(long.Parse(cnpj))
        {

        }

        public Cnpj SetValue(long cnpj)
        {
            return new Cnpj(cnpj);
        }

        public Cnpj SetValue(int cnpj)
        {
            return SetValue((long)cnpj);
        }

        public Cnpj SetValue(string cnpj)
        {
            return SetValue(long.Parse(cnpj));
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}
