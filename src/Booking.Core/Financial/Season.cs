﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Booking
{
    public static class SeasonRules
    {
        #region Validation Properties
        public const int MaxDescriptionLength = 254;
        #endregion
    }

    [Table("seasons", Schema = "financial")]
    public class Season : Entity<long>
    {

        #region Business Properties
        [Required]
        public virtual DateTime Start { get; set; }

        [Required]
        public virtual DateTime Finish { get; set; }

        [Required]
        [MaxLength(SeasonRules.MaxDescriptionLength)]
        public virtual string Description { get; set; }
        #endregion

        #region Constructors
        public Season()
        {

        }

        public Season(DateTime start, DateTime finish, string description): this()
        {
            Start = start;
            Finish = finish;
            Description = description;
        }
        #endregion
    }
}
