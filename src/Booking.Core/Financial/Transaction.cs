﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using Abp.Timing;
using Booking.Authorization.Users;
using Booking.Booking;
using Booking.Places;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

// @Index(name="transaction_search_idx", columns={"mature_at", "confirmation_at", "season_id"}),
// * @Index(name= "transaction_report_idx", columns={ "mature_at", "confirmation_at", "season_id", "place_id"})

namespace Booking.Financial
{
    public static class TransactionErrors
    {
        #region Exceptions
        public const string MatureDateCanNotBeHigherThanConfirmationDate = "finantial.transaction.matureCanNotBeHigherThanConfirmation";
        public const string ConfirmationDateCanNotBeLowerThanMatureDate = "finantial.transaction.confirmationCanNotBeLowerThanMature";
        #endregion
    }

    public static class TransactionRules
    {
        public const int MaxDescriptionLength = 32 * 1024;
    }

    [Table("transactions", Schema = "financial")]
    public class Transaction : Entity<long>, IFullAudited
    {

        #region Audit Properties
        public virtual long? DeleterUserId { get; set; }
        public virtual DateTime? DeletionTime { get; set; }
        public virtual bool IsDeleted { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? CreatorUserId { get; set; }
        public virtual DateTime CreationTime { get; set; }

        #endregion

        #region Business Properties
        [Required]
        public virtual DateTime MatureAt { get; set; }

        public virtual DateTime? ConfirmationAt { get; set; }

        [Required]
        public virtual double Ammount { get; set; }

        [MaxLength(TransactionRules.MaxDescriptionLength)]
        public virtual string Description { get; set; }

        #endregion

        #region Relational Properties
        public virtual Place Place { get; set; }
        public virtual long? PlaceId { get; set; }

        public virtual Booking.Booking Booking { get; set; }
        public virtual long? BookingId { get; set; }

        public virtual User User { get; set; }
        public virtual long? UserId { get; set; }

        public virtual Season Season { get; set; }
        public virtual long SeasonId { get; set; }

        public virtual Wallet Wallet { get; set; }
        public virtual long WalletId { get; set; }

        public virtual UserFeeBooking Fee { get; set; }
        public virtual long? FeeId { get; set; }
        #endregion

        #region Parent
        public virtual Transaction Parent { get; set; }
        public virtual long? ParentId { get; set; }
        #endregion

        #region Constructors
        public Transaction() {
            CreationTime = Clock.Now;
        }

        public Transaction(double ammount, DateTime matureAt): this()
        {
            Ammount = ammount;
            MatureAt = matureAt;
        }

        public Transaction(double ammount, DateTime matureAt, DateTime confirmationAt): this(ammount, matureAt)
        {
            ConfirmationAt = confirmationAt;
        }

        public Transaction(double ammount, DateTime matureAt, DateTime confirmationAt, Wallet wallet, Season season): this(ammount, matureAt, confirmationAt)
        {
            Wallet = Wallet;
            Season = season;
        }

        #endregion

        #region Properties Methods
        public void SetFee(UserFee fee)
        {
            Fee = new UserFeeBooking(Booking, fee);
        }
        #endregion
    }
}
