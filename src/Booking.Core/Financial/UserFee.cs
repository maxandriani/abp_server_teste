﻿using Abp.Domain.Entities;
using Booking.Authorization.Users;
using Booking.Booking;
using Booking.Places;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Financial
{
    public static class UserFeeRules
    {
        #region Validation Properties
        public const double DefaultPercentFee = 0.2;
        #endregion
    }

    [Table("user_fee", Schema = "financial")]
    public class UserFee : Entity<long>
    {
        
        #region Business properties

        [Required]
        public virtual double Percent { get; set; }

        #endregion

        #region Relational Properties
        [Required]
        public virtual User User { get; set; }
        public virtual long UserId { get; set; }

        public virtual Place Place { get; set; }
        public virtual long? PlaceId { get; set; }

        public virtual ICollection<UserFeeBooking> BookingFees { get; set; }
        #endregion

        #region Constructors
        public UserFee()
        {

        }

        public UserFee(double percent, Place place, User user): this()
        {
            Percent = percent;
            Place = place;
            User = user;
        }

        public UserFee(Place place, User user): this(UserFeeRules.DefaultPercentFee, place, user)
        {

        }

        #endregion
    }
}
