﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Financial
{
    public static class WalletRules
    {
        #region Validation Properties
        public const int MaxDescriptionLength = 256;
        #endregion
    }

    [Table("wallets", Schema = "financial")]
    public class Wallet : Entity<long>
    {

        #region Business properties
        public virtual bool Default { get; set; } = false;

        [Required]
        [MaxLength(WalletRules.MaxDescriptionLength)]
        public virtual string Description { get; set; }

        #endregion

        #region Constructors
        public Wallet()
        {

        }

        public Wallet(string description, bool def): this()
        {
            Description = description;
            Default = def;
        }

        public Wallet(string description): this(description, false)
        {

        }
        #endregion
    }
}
