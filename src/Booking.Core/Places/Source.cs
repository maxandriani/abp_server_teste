﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Places
{
    public static class SourceRules
    {
        #region Validation Properties
        public const int MaxNameLength = 32;
        public const int MaxUrlLength = 32 * 1024; // 32 kb
        #endregion
    }

    [Table("sources", Schema = "places")]
    public class Source : Entity<long>
    {
        #region Business properties
        [StringLength(SourceRules.MaxNameLength)]
        public virtual string Name { get; set; }

        [StringLength(SourceRules.MaxUrlLength)]
        [Required]
        [Url]
        public virtual string Url { get; set; }
        #endregion

        #region Relational properties
        public virtual Place Place { get; set; }
        
        public virtual long PlaceId { get; set; }
        #endregion

        #region Constructors
        public Source()
        {
        }

        public Source(string name, string url) : this()
        {
            Name = name;
            Url = url;
        }

        public Source(string name, string url, Place place) : this(name, url)
        {
            Place = place;
        }
        #endregion

    }
}
