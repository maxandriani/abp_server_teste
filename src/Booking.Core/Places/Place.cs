﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using Booking.Financial;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Booking.Places
{
    public static class PlaceRules
    {
        #region Validation Properties
        public const int MaxNameLength = 32;
        public const int MaxDescriptionLength = 254;
        #endregion
    }

    [Table("places", Schema = "places")]
    public class Place : Entity<long>, IFullAudited
    {
        #region Audition properties
        public virtual long? CreatorUserId { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? DeleterUserId { get; set; }
        public virtual DateTime? DeletionTime { get; set; }
        public virtual bool IsDeleted { get; set; }
        #endregion

        #region Business properties
        [StringLength(PlaceRules.MaxNameLength)]
        [Required]
        public virtual string Name { get; set; }

        [StringLength(PlaceRules.MaxDescriptionLength)]
        public virtual string Description { get; set; }
        #endregion

        #region Relational properties
        public virtual ICollection<Source> Sources { get; set; }
        
        public virtual ICollection<Transaction> Transactions { get; set; }

        public virtual ICollection<UserFee> Fees { get; set; }
        #endregion

        #region Constructors
        public Place()
        {
            CreationTime = Clock.Now;
        }

        public Place(string name, string description) : this()
        {
            Name = name;
            Description = description;
        }
        #endregion
    }
}
