using Microsoft.AspNetCore.Antiforgery;
using Booking.Controllers;

namespace Booking.Web.Host.Controllers
{
    public class AntiForgeryController : BookingControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
