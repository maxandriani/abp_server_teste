﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Booking.Authorization.Roles;
using Booking.Authorization.Users;
using Booking.Booking;
using Booking.MultiTenancy;
using Abp.Localization;
using Booking.Financial;
using Booking.Places;
using System;

namespace Booking.EntityFrameworkCore
{
    public class BookingDbContext : AbpZeroDbContext<Tenant, Role, User, BookingDbContext>
    {
        /* Define a DbSet for each entity of the application */
        #region Booking
        public DbSet<Booking.Booking> Booking { get; set; }
        public DbSet<BookingPhone> BookingPhone { get; set; }
        #endregion

        #region Finantial
        public DbSet<Season> Season { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<UserFee> UserFee { get; set; }
        public DbSet<UserFeeBooking> UserFeeBooking { get; set; }
        public DbSet<Wallet> Wallet { get; set; }
        #endregion
         
        #region Places
        public DbSet<Place> Place { get; set; }
        public DbSet<Source> Source { get; set; }
        #endregion

        public BookingDbContext(DbContextOptions<BookingDbContext> options)
            : base(options)
        {
        }

        // add these lines to override max length of property
        // we should set max length smaller than the PostgreSQL allowed size (10485760)
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationLanguageText>()
                .Property(p => p.Value)
                .HasMaxLength(100); // any integer that is smaller than 10485760

            #region Booking
            // @Index(name="booking_search_idx", columns={"start", "finish", "place_id"})
            modelBuilder.Entity<Booking.Booking>()
                .HasIndex(b => new { b.Start, b.Finish, b.PlaceId })
                .HasName("booking_search_idx");
            modelBuilder.Entity<BookingPhone>()
                .OwnsOne(m => m.Phone, a =>
                    {
                        a.Property(p => p.Ddi)
                            .HasMaxLength(3)
                            .HasColumnName("PhoneDdi");
                        a.Property(p => p.Ddd)
                            .HasMaxLength(3)
                            .HasColumnName("PhoneDdd");
                        a.Property(p => p.Number)
                            .HasMaxLength(12)
                            .HasColumnName("PhoneNumber");
                    });
            // @Unique(name="user_fee_booking_composite_key", columns={userId, bookingId})
            modelBuilder.Entity<UserFeeBooking>()
                .HasIndex(ufb => new { ufb.UserId, ufb.BookingId })
                .HasName("user_fee_booking_composite_key")
                .IsUnique();
            #endregion

            #region Finantial
            // @Index(name="season_search_idx", columns={"start", "finish"})
            modelBuilder.Entity<Season>()
                .HasIndex(s => new { s.Start, s.Finish })
                .HasName("season_search_idx");
            // @Index(name="transaction_search_idx", columns={"mature_at", "confirmation_at", "season_id"}),
            modelBuilder.Entity<Transaction>()
                .HasIndex(t => new { t.MatureAt, t.ConfirmationAt, t.SeasonId })
                .HasName("transaction_search_idx");
            // @Index(name = "transaction_report_idx", columns ={ "mature_at", "confirmation_at", "season_id", "place_id"})
            modelBuilder.Entity<Transaction>()
                .HasIndex(t => new { t.MatureAt, t.ConfirmationAt, t.SeasonId, t.PlaceId })
                .HasName("transaction_report_idx");
            // @Index(name="wallet_default_idx", columns={ "default" })
            modelBuilder.Entity<Wallet>()
                .HasIndex(w => w.Default)
                .HasName("wallet_default_idx");
            #endregion

            #region Places
            modelBuilder.Entity<Source>()
                .HasOne(s => s.Place)
                .WithMany(p => p.Sources)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion
        }
    }

}
