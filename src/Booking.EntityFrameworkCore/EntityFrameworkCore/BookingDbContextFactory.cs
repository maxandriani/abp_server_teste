﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Booking.Configuration;
using Booking.Web;

namespace Booking.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class BookingDbContextFactory: IDesignTimeDbContextFactory<BookingDbContext>
    {
        public BookingDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<BookingDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            BookingDbContextConfigurer.Configure(builder, configuration.GetConnectionString(BookingConsts.ConnectionStringName));

            return new BookingDbContext(builder.Options);
        }
    }
}
