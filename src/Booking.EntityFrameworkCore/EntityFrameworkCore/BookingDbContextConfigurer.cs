using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Booking.EntityFrameworkCore
{
    public static class BookingDbContextConfigurer
    {

        public static void Configure(DbContextOptionsBuilder<BookingDbContext> builder, string connectionString)
        {
            builder
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<BookingDbContext> builder, DbConnection connection)
        {
            builder
                .UseLazyLoadingProxies()
                .UseNpgsql(connection);
        }
    }
}
